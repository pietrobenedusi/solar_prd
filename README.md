# README #

Parallel implementation for PRD radiative transfer in 1D

### Library Dependencies ###
* PETSc 
* MPI

### Input Dependencies ###
* input files encoding the atmospheric model 

### Compile ###

cd bin && make 

(PETSC_DIR should be defined for the Makefile to compile)

### Run ###
For example, using four cores:

mpirun -n 4 ./main

