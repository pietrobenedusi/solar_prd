#include "RT_problem.hpp"

void RT_problem::read_atom(std::string filename){

	if (mpi_rank_ == 0) std::cout << "Reading atom data from " << filename << std::endl;

	std::ifstream myFile(filename);
	std::string line;	

	if (not myFile.good()) std::cerr << "\nERROR: File " << filename << " does not exist!\n" << std::endl;

	int counter = 0;

	while(getline(myFile, line))
	{
		std::istringstream lineStream(line);
		
		std::string line_entry;

		switch(counter) {
    		case 0 : lineStream >> mass_; 
    			     lineStream >> line_entry;
    			     if (line_entry.compare("!Atomic") != 0) std::cout << "WARNING: " << line_entry << " is not !Atomic" << '\n';
            	break;      
    		
    		case 1 : lineStream >> El_;
    				 lineStream >> Jl2_;
    				 lineStream >> gl_;
    				 lineStream >> line_entry;
    				 // if (line_entry.compare("!El[cm-1]") != 0) std::cout << "WARNING: " << line_entry << " is not !El[cm-1]" << '\n';
             	break;
            
            case 2 : lineStream >> Eu_;
    				 lineStream >> Ju2_;
    				 lineStream >> gu_;
    				 lineStream >> line_entry;
    				 // if (line_entry.compare("!Eu[cm-1]") != 0) std::cout << "WARNING: " << line_entry << " is not !Eu[cm-1]" << '\n';
             	break;
            
            case 3 : lineStream >> Aul_;
            		 lineStream >> line_entry;
            		 if (line_entry.compare("!Aul[s-1]") != 0) std::cout << "WARNING: " << line_entry << " is not !Aul[s-1]" << '\n';
             	break;
		}

		counter++;
	}
}


void RT_problem::read_atmosphere(std::string filename){
	
	if (mpi_rank_ == 0) std::cout << "Reading atmospheric data from " << filename << std::endl;

	std::ifstream myFile(filename);
	std::string line;	

	if (not myFile.good()) std::cerr << "\nERROR: File " << filename << " does not exist!\n" << std::endl;

	bool first_line = true;

	while(getline(myFile, line))
	{
		std::istringstream lineStream(line);
		double entry;
		std::string line_entry;

		if (first_line) // skip first line 
		{
			lineStream >> line_entry;
			if (line_entry.compare("Height[km]") != 0) std::cout << line_entry << " is not Height[km]" << '\n';
			// std::cout << "reading "<< line_entry << '\n';
			lineStream >> line_entry;
			if (line_entry.compare("Temp[K]") != 0)    std::cout << line_entry << " is not Temp[K]" << '\n';
			// std::cout << "reading "<< line_entry << '\n';
			lineStream >> line_entry;
			if (line_entry.compare("Vmic[cm/s]") != 0) std::cout << line_entry << " is not Vmic[cm/s]" << '\n';
			// std::cout << "reading "<< line_entry << '\n';
			lineStream >> line_entry;
			if (line_entry.compare("Damp") != 0)       std::cout << line_entry << " Damp" << '\n';
			// std::cout << "reading "<< line_entry << '\n';
			lineStream >> line_entry;
			if (line_entry.compare("Nl[cm-3]") != 0)   std::cout << line_entry << " is not Nl[cm-3]" << '\n';
			// std::cout << "reading "<< line_entry << '\n';
			lineStream >> line_entry;
			if (line_entry.compare("Nu[cm-3]") != 0)   std::cout << line_entry << " is not Nu[cm-3]" << '\n';
			// std::cout << "reading "<< line_entry << '\n';
			lineStream >> line_entry;
			if (line_entry.compare("Cul[s-1]") != 0)   std::cout << line_entry << " is not Cul[s-1]" << '\n';			
			// std::cout << "reading "<< line_entry << '\n';
			lineStream >> line_entry;
			if (line_entry.compare("Qel[s-1]") != 0)   std::cout << line_entry << " is not Qel[s-1]" << '\n';			
			// std::cout << "reading "<< line_entry << '\n';
		}
		else
		{	
			lineStream >> entry;		
			depth_grid_.push_back(entry);
			lineStream >> entry;
			T_.push_back(entry);
			lineStream >> entry;
			xi_.push_back(entry);
			lineStream >> entry;
			a_.push_back(entry);
			lineStream >> entry;
			Nl_.push_back(entry);
			lineStream >> entry;
			Nu_.push_back(entry);
			lineStream >> entry;
			Cul_.push_back(entry);
			lineStream >> entry;
			Qel_.push_back(entry);			
		}		

		first_line = false;
	} 	
}


void RT_problem::read_bulk_velocity(std::string filename){
	
	if (mpi_rank_ == 0) std::cout << "Reading bulk velocities from " << filename << std::endl;

	std::ifstream myFile(filename);
	std::string line;	

	if (not myFile.good()) std::cerr << "\nERROR: File " << filename << " does not exist!\n" << std::endl;

	bool first_line = true;

	while(getline(myFile, line))
	{
		std::istringstream lineStream(line);
		double entry;
		std::string line_entry;

		if (first_line) // skip first line 
		{
			lineStream >> line_entry;
			if (line_entry.compare("V[kms-1]") != 0) std::cout << line_entry << " is not V[kms-1]" << '\n';
			lineStream >> line_entry;
			if (line_entry.compare("theta[rad]") != 0)    std::cout << line_entry << " is not theta[rad]" << '\n';
			lineStream >> line_entry;
			if (line_entry.compare("chi[rad]") != 0) std::cout << line_entry << " is not chi[rad]" << '\n';			
		}
		else
		{	
			lineStream >> entry;			
			v_b_.push_back(1e5 * entry); // conversion to cm
			lineStream >> entry;
			theta_v_b_.push_back(entry);
			lineStream >> entry;
			chi_v_b_.push_back(entry);			
		}		

		first_line = false;
	} 
}


void RT_problem::read_frequency(std::string filename){

	if (mpi_rank_ == 0) std::cout << "Reading frequencies [s-1] from " << filename << std::endl;

	std::ifstream myFile(filename);
	std::string line;	

	if (not myFile.good()) std::cerr << "\nERROR: File " << filename << " does not exist!\n" << std::endl;

	bool first_line = true;

	while(getline(myFile, line))
	{
		std::istringstream lineStream(line);
		double entry;
		std::string line_entry;

		if (not first_line) // skip first line 
		{		
			lineStream >> entry;			
			lineStream >> entry;
			nu_grid_.push_back(entry);		

			if (entry < 1.0e14) std::cerr << "\nWARNING: frequency: " << entry << " probably not in Herz[s-1]!\n" << std::endl;
		}		

		first_line = false;
	} 
}


void RT_problem::read_magnetic_field(std::string filename){

	if (mpi_rank_ == 0) std::cout << "Reading magnetic field from " << filename << std::endl;

	std::ifstream myFile(filename);
	std::string line;	

	if (not myFile.good()) std::cerr << "\nERROR: File " << filename << " does not exist!\n" << std::endl;

	bool first_line = true;

	while(getline(myFile, line))
	{
		std::istringstream lineStream(line);
		double entry;
		std::string line_entry;

		if (first_line) // skip first line 
		{
			lineStream >> line_entry;
			if (line_entry.compare("B[G]") != 0) std::cout << line_entry << " is not B[G]" << '\n';
			lineStream >> line_entry;
			if (line_entry.compare("theta[rad]") != 0)    std::cout << line_entry << " is not theta[rad]" << '\n';
			lineStream >> line_entry;
			if (line_entry.compare("chi[rad]") != 0) std::cout << line_entry << " is not chi[rad]" << '\n';			
		}
		else
		{	
			lineStream >> entry;	
			entry *= 1399600; // convert to Larmor frequency
			nu_L_.push_back(entry);

			lineStream >> entry;
			theta_B_.push_back(entry);

			lineStream >> entry;
			chi_B_.push_back(entry);			
		}		

		first_line = false;
	} 
}


void RT_problem::read_sigma(std::string filename){

	if (mpi_rank_ == 0) std::cout << "Reading sigma from " << filename << std::endl;

	std::ifstream myFile(filename);
	std::string line;	

	if (not myFile.good()) std::cerr << "\nERROR: File " << filename << " does not exist!\n" << std::endl;

	bool first_line = true;

	while(getline(myFile, line))
	{	
		std::stringstream iss(line);
		double entry;
	
		if (not first_line) while (iss >> entry) sigma_.push_back(entry);					

		first_line = false;
	} 
}


void RT_problem::read_k_c(std::string filename){

	if (mpi_rank_ == 0) std::cout << "Reading k_c from " << filename << std::endl;

	std::ifstream myFile(filename);
	std::string line;	

	if (not myFile.good()) std::cerr << "\nERROR: File " << filename << " does not exist!\n" << std::endl;

	bool first_line = true;

	while(getline(myFile, line))
	{	
		std::stringstream iss(line);
		double entry;

		if (not first_line) while (iss >> entry) k_c_.push_back(entry);					

		first_line = false;
	} 
}
	

void RT_problem::read_eps_c_th(std::string filename){

	if (mpi_rank_ == 0) std::cout << "Reading eps_c_th from " << filename << std::endl;

	std::ifstream myFile(filename);
	std::string line;	

	if (not myFile.good()) std::cerr << "\nERROR: File " << filename << " does not exist!\n" << std::endl;

	bool first_line = true;

	while(getline(myFile, line))
	{	
		std::stringstream iss(line);
		double entry;

		if (not first_line) while (iss >> entry) eps_c_th_.push_back(entry);					

		first_line = false;
	} 
}


void RT_problem::set_theta_chi_grids(const size_t N_theta, const size_t N_chi, const bool double_GL){

	if (mpi_rank_ == 0 and N_theta % 2 != 0) std::cerr << "\n========= WARNING: N_theta odd! =========\n" << std::endl;

	// init theta and mu grids and weights 
    // legendre_rule(N_theta,  0.0, pi_, theta_grid_, w_theta_);

    if (double_GL)    	
    {
    	std::vector<double> mu_1;
    	std::vector<double> mu_2;

    	std::vector<double> w_1;
    	std::vector<double> w_2;

    	legendre_rule(N_theta / 2,  -1.0, 0.0, mu_1, w_1);
    	legendre_rule(N_theta / 2,   0.0, 1.0, mu_2, w_2);

    	// first half
    	for (size_t i = 0; i < N_theta / 2; ++i)
	    {
	    	mu_grid_.push_back(mu_1[i]);
	    	w_theta_.push_back(w_1[i]);

			theta_grid_.push_back(std::acos(mu_1[i]));		    	
	    }

	    // second half
    	for (size_t i = 0; i < N_theta / 2; ++i)
	    {
	    	mu_grid_.push_back(mu_2[i]);
	    	w_theta_.push_back(w_2[i]);

			theta_grid_.push_back(std::acos(mu_2[i]));		    	
	    }
    }
    else
    {
    	legendre_rule(N_theta,  -1.0, 1.0, mu_grid_, w_theta_);

	    for (size_t i = 0; i < N_theta; ++i)
	    {
	    	theta_grid_.push_back(std::acos(mu_grid_[i]));
	    }
    }

    // init equidistant chi grid in [0, 2pi] and trap weights
    const double delta_chi = 2.0 * pi_ / N_chi;

    for (size_t i = 0; i < N_chi; ++i)
    {
    	chi_grid_.push_back(i * delta_chi);	    	
    	w_chi_.push_back(delta_chi);
    }    
}


// to use after input are read
void RT_problem::refine_atmosphere()
{
	depth_grid_ = refine_vector(depth_grid_);
	T_   = refine_vector(T_);
	xi_  = refine_vector(xi_);
	a_   = refine_vector(a_);
	Nl_  = refine_vector(Nl_);
	Nu_  = refine_vector(Nu_);
	Cul_ = refine_vector(Cul_);
	Qel_ = refine_vector(Qel_);

	nu_L_    = refine_vector(nu_L_);
	theta_B_ = refine_vector(theta_B_);
	chi_B_   = refine_vector(chi_B_);

	v_b_       = refine_vector(v_b_);
	theta_v_b_ = refine_vector(theta_v_b_); 
	chi_v_b_   = refine_vector(chi_v_b_); 

	// // HACK to reduce size for N_s = 128
	// for (int i = 0; i < 11; ++i)
	// {
	// 	depth_grid_.erase(depth_grid_.begin());
	// 	T_.erase(T_.begin());
	// 	xi_.erase(xi_.begin());
	// 	a_.erase(a_.begin());
	// 	Nl_.erase(Nl_.begin());
	// 	Nu_.erase(Nu_.begin());
	// 	Cul_.erase(Cul_.begin());
	// 	Qel_.erase(Qel_.begin());

	// 	nu_L_.erase(nu_L_.begin());
	// 	theta_B_.erase(theta_B_.begin());
	// 	chi_B_.erase(chi_B_.begin());

	// 	v_b_.erase(v_b_.begin());
	// 	theta_v_b_.erase(theta_v_b_.begin());
	// 	chi_v_b_.erase(chi_v_b_.begin());
	// }

	// continuum
	const size_t block_s = nu_grid_.size();

	sigma_    = refine_vector_blocked(sigma_, block_s); 
	k_c_      = refine_vector_blocked(k_c_, block_s); 
	eps_c_th_ = refine_vector_blocked(eps_c_th_, block_s); 

	// // HACK to reduce size 
	// for (int i = 0; i < 11 * block_s; ++i)
	// {
	// 	sigma_.erase(sigma_.begin());
	// 	k_c_.erase(k_c_.begin());
	// 	eps_c_th_.erase(eps_c_th_.begin());
	// }
}

void RT_problem::refine_frequency(){

	const size_t N_nu_cr = size(nu_grid_);

	nu_grid_  = refine_vector(nu_grid_);

	eps_c_th_ = refine_vector_blocked2(eps_c_th_, N_nu_cr); 
	k_c_      = refine_vector_blocked2(k_c_, N_nu_cr);
	sigma_    = refine_vector_blocked2(sigma_, N_nu_cr);
}


void RT_problem::set_sizes(){

	// set disc. parameters from input		
	N_s_     = depth_grid_.size();
	N_nu_	 = nu_grid_.size();
	N_theta_ = theta_grid_.size();
	N_chi_	 = chi_grid_.size();

	block_size_ = 4 * N_nu_ * N_theta_ * N_chi_;
	tot_size_   = N_s_ * block_size_;

	if (mpi_rank_ == 0 and mpi_size_ > (int) N_s_) std::cerr << "\n========= WARNING: mpi_size > N_s! =========\n" << std::endl;
}


void RT_problem::set_up(){
 
    if (mpi_rank_ == 0) std::cout << "\nPrecomputing quantities...";				

    // temporary constants
    double tmp_const, tmp_const2, tmp_const3;
    
	// compute line-center frequency
	const double dE = Eu_ - El_;
	nu_0_ = dE * c_;

	// compute coefficients depending on the spatial point //////////////////////////

	// init
	D1_.resize(N_s_);
	D2_.resize(N_s_);
	Doppler_width_.resize(N_s_);
	k_L_.resize(N_s_);
	epsilon_.resize(N_s_);
	W_T_.resize(N_s_);

	// const for k_L
	tmp_const = c_ * c_* (Ju2_ + 1) * Aul_ / (8 * pi_ * nu_0_ * nu_0_ * (Jl2_ + 1));

	// const for Wien 
	tmp_const2 = 2 * h_ * nu_0_ * nu_0_ * nu_0_ / (c_ * c_);
	
	// const for D1
	tmp_const3 = (Ju2_ * Ju2_ + 2 * Ju2_ - 3) / ( 3 * (Ju2_ * Ju2_ + Ju2_ - 7));

	const double mass_real = mass_ * 1.6605e-24;

	// fill
	for (size_t i = 0; i < N_s_; ++i)
	{		

		D2_[i] = 0.5 * Qel_[i];

		D1_[i] = tmp_const3 * D2_[i];

		k_L_[i] = tmp_const * Nl_[i];

		Doppler_width_[i] = dE * std::sqrt(2 * k_B_ * T_[i] / mass_real + xi_[i] * xi_[i]);

		epsilon_[i] = Cul_[i]/(Cul_[i] + Aul_);	

		W_T_[i] = tmp_const2 * std::exp(- h_ * nu_0_ / (k_B_ * T_[i]));
		
		for (size_t n = 0; n < N_nu_; ++n)
		{
			u_.push_back((nu_0_ - nu_grid_[n]) / Doppler_width_[i]);
		}
	}	
	    
	// precompute polarization tensors T_KQ	/////////////////////////////
	for (int i = 0; i < 4; ++i)
	{
		for (size_t j = 0; j < N_theta_; ++j)
		{
			for (size_t k = 0; k < N_chi_; ++k)
			{
				auto T_KQ = compute_T_KQ(i, theta_grid_[j], chi_grid_[k]);
			
				T_KQ_.push_back(T_KQ);							
			}
		}
	}
			
	// precompute etas and rhos //////////////////////////////////////////
	PetscErrorCode ierr;

	int istart, iend;
	int ix[4];

	bool dichroism_warning = false;

	ierr = VecGetOwnershipRange(eta_field_, &istart, &iend);CHKERRV(ierr);
	
	std::vector<double> etas_and_rhos;

	size_t i_loc, j_loc = 0, k_loc = 0, n_loc = 0;
	
	for (int i = istart; i < iend; i = i + 4) 
	{	
		i_loc = i/block_size_;
		
		etas_and_rhos = compute_eta_and_rhos(i_loc, j_loc, k_loc, n_loc);	

		n_loc++;

		if (n_loc == N_nu_)
		{
			n_loc = 0;
			k_loc++;

			if (k_loc == N_chi_)
			{
				k_loc = 0;
				j_loc++;

				if (j_loc == N_theta_) j_loc = 0;				
			}
		}		

		// assign ix[ii] = i + ii;
		std::iota(ix, ix + 4, i);

		// debug		
		if (etas_and_rhos[0] <= 0)  std::cerr << "\nWARNING: eta_I not positive!" << std::endl; 

		const double dichroism_module = std::sqrt(etas_and_rhos[1] * etas_and_rhos[1] + etas_and_rhos[2] * etas_and_rhos[2] + etas_and_rhos[3] * etas_and_rhos[3]);
		
		if (etas_and_rhos[0] < dichroism_module) dichroism_warning = true;
		// {			
		// 	std::cout << "eta_I = " << etas_and_rhos[0] << std::endl;
		// 	std::cout << "dichroism_module = " << dichroism_module << std::endl;
		// } 
				
		ierr = VecSetValues(eta_field_, 4, ix, &etas_and_rhos[0], INSERT_VALUES);CHKERRV(ierr); 
		ierr = VecSetValues(rho_field_, 4, ix, &etas_and_rhos[4], INSERT_VALUES);CHKERRV(ierr); 		
	}

	ierr = VecAssemblyBegin(eta_field_);CHKERRV(ierr); 
	ierr = VecAssemblyEnd(eta_field_);CHKERRV(ierr); 
	ierr = VecAssemblyBegin(rho_field_);CHKERRV(ierr); 
	ierr = VecAssemblyEnd(rho_field_);CHKERRV(ierr); 

	if (dichroism_warning) std::cout << "\nWARNING: eta_I < eta! (Eq. (7) Gioele Paganini 2018, Part III)" << std::endl; 	

	// precomputing dtau /////////////////////////////////////////////////////////////

	const int stoke_block = block_size_ / 4; // stoke_block = N_chi_ * N_theta * N_nu

	int eta_start, eta_end;

	ierr = VecGetOwnershipRange(eta_field_, &eta_start, &eta_end);CHKERRV(ierr);

	// copy the portion of eta_field_ that is needed //////////
	const bool not_last_rank = mpi_rank_ < mpi_size_ - 1;

	// create vector for the first portion of eta own by the next processor
	Vec next_eta; 		

	ierr = VecCreate(PETSC_COMM_WORLD, &next_eta);CHKERRV(ierr);	
	ierr = VecSetSizes(next_eta, stoke_block, mpi_size_ * stoke_block);CHKERRV(ierr);		
	ierr = VecSetFromOptions(next_eta);CHKERRV(ierr);				
					
	IS is_send, is_recive;

	if (not_last_rank)	
	{
		ierr = ISCreateStride(PETSC_COMM_SELF, stoke_block, eta_end, 4, &is_send);CHKERRV(ierr);
		ierr = ISCreateStride(PETSC_COMM_SELF, stoke_block, stoke_block * mpi_rank_, 1, &is_recive);CHKERRV(ierr);
	}
	else
	{
		ierr = ISCreateStride(PETSC_COMM_SELF, 0, 0, 1, &is_send);CHKERRV(ierr);
		ierr = ISCreateStride(PETSC_COMM_SELF, 0, 0, 1, &is_recive);CHKERRV(ierr);
	}
		
	VecScatter scatter_ctx;	

	ierr = VecScatterCreate(eta_field_,is_send,next_eta,is_recive, &scatter_ctx);CHKERRV(ierr);
	ierr = VecScatterBegin(scatter_ctx, eta_field_, next_eta, INSERT_VALUES,SCATTER_FORWARD);CHKERRV(ierr);
    ierr = VecScatterEnd(scatter_ctx, eta_field_, next_eta, INSERT_VALUES,SCATTER_FORWARD);CHKERRV(ierr);
    
    // clean
    ierr = VecScatterDestroy(&scatter_ctx);CHKERRV(ierr);
    ierr = ISDestroy(&is_send);CHKERRV(ierr);
	ierr = ISDestroy(&is_recive);CHKERRV(ierr);

    /////////////// scattering done ///////////////
	
    ierr = VecGetOwnershipRange(dtau_, &istart, &iend);CHKERRV(ierr);    

    int start_next, end_next, counter = 0;
    ierr = VecGetOwnershipRange(next_eta, &start_next, &end_next);CHKERRV(ierr);    
          
	double eta1, eta2, dtau;
	int index;		

	bool sign_err = false; 
	
	for (int i = istart; i < iend; ++i) 
	{
		i_loc = i/stoke_block;	
		
		index = i * 4;
		ierr = VecGetValues(eta_field_, 1, &index, &eta1);CHKERRV(ierr);

		index += block_size_;

		if (index < eta_end)
		{
			ierr = VecGetValues(eta_field_, 1, &index, &eta2);	CHKERRV(ierr);

			dtau = 0.5 * (eta1 + eta2) * 1e5 * std::abs(depth_grid_[i_loc + 1] - depth_grid_[i_loc]); // conversion to cm						

			ierr = VecSetValue(dtau_, i, dtau, INSERT_VALUES);CHKERRV(ierr);

			if (dtau <= 0) sign_err = true; 

		}
		else if (not_last_rank)// use data copied from next processor
		{							
			index = start_next + counter;
			ierr = VecGetValues(next_eta, 1, &index, &eta2);CHKERRV(ierr);	
			counter++;

			dtau = 0.5 * (eta1 + eta2) * 1e5 * std::abs(depth_grid_[i_loc + 1] - depth_grid_[i_loc]); // conversion to cm								

			ierr = VecSetValue(dtau_, i, dtau, INSERT_VALUES);CHKERRV(ierr);

			if (dtau <= 0) sign_err = true; 
		}				
	}

	if (sign_err) std::cerr << "\nWARNING: dtau negative!" << std::endl; 

	ierr = VecAssemblyBegin(dtau_);CHKERRV(ierr); 
	ierr = VecAssemblyEnd(dtau_);CHKERRV(ierr); 

	// save_vec(dtau_, "../output/dtau.m","dt");

	// clean	
	ierr = VecDestroy(&next_eta);CHKERRV(ierr); 		

	if (mpi_rank_ == 0) std::cout << "done" << std::endl;
	
	// TODO others precomputation? recurring constants?
}


void RT_problem::init_fields(){

	PetscErrorCode ierr;
	
	// Stokes vector I
	ierr = VecCreate(PETSC_COMM_WORLD, &I_field_);CHKERRV(ierr);		
	ierr = VecSetSizes(I_field_,PETSC_DECIDE,tot_size_);CHKERRV(ierr);		
	ierr = VecSetBlockSize(I_field_,block_size_);CHKERRV(ierr);
	ierr = VecSetFromOptions(I_field_);CHKERRV(ierr);	
	
	// source function S
	ierr = VecCreate(PETSC_COMM_WORLD, &S_field_);CHKERRV(ierr);		
	ierr = VecSetSizes(S_field_,PETSC_DECIDE,tot_size_);CHKERRV(ierr);		
	ierr = VecSetBlockSize(S_field_,block_size_);CHKERRV(ierr);
	ierr = VecSetFromOptions(S_field_);CHKERRV(ierr);
	
	// eta
	ierr = VecCreate(PETSC_COMM_WORLD, &eta_field_);CHKERRV(ierr);		
	ierr = VecSetSizes(eta_field_,PETSC_DECIDE,tot_size_);CHKERRV(ierr);		
	ierr = VecSetBlockSize(eta_field_,block_size_);CHKERRV(ierr);
	ierr = VecSetFromOptions(eta_field_);CHKERRV(ierr);
	
	// rho
	ierr = VecCreate(PETSC_COMM_WORLD, &rho_field_);CHKERRV(ierr);		
	ierr = VecSetSizes(rho_field_,PETSC_DECIDE,tot_size_);CHKERRV(ierr);		
	ierr = VecSetBlockSize(rho_field_,block_size_);CHKERRV(ierr);
	ierr = VecSetFromOptions(rho_field_);CHKERRV(ierr);
	
	// dtau
  	int n_loc;
  	ierr = VecGetLocalSize(eta_field_, &n_loc);CHKERRV(ierr);
	ierr = VecCreate(PETSC_COMM_WORLD, &dtau_);CHKERRV(ierr);		
	ierr = VecSetSizes(dtau_,n_loc/4,tot_size_/4);CHKERRV(ierr);			
	ierr = VecSetFromOptions(dtau_);CHKERRV(ierr);	
}


std::vector<double> RT_problem::get_field(const size_t i, const size_t j, const size_t k, const size_t n, const Vec &field){

	std::vector<double> out_vec(4);	

	PetscErrorCode ierr;

	const size_t index = local_to_global(i,  j,  k,  n);

	int ix[4];
	std::iota(ix, ix + 4, index);

	ierr = VecGetValues(field, 4, ix, &out_vec[0]);

	if (ierr) std::cerr << "ERROR in get_field()!"<< std::endl;	

	return out_vec;
}


double RT_problem::get_field_scalar(const size_t i, const size_t j, const size_t k, const size_t n, const Vec &field){
	
	PetscErrorCode ierr;

	int index = i * block_size_ / 4 + j * (N_chi_ * N_nu_) + k * N_nu_ + n;
	
	double out_value;

	ierr = VecGetValues(field, 1, &index, &out_value);CHKERRQ(ierr); 

	return out_value;	
}


// compute u_b = (nu_0/c) * v_b_.Omega / Doppler_width 
double RT_problem::compute_u_b(size_t i, double theta, double chi){

	if (i >= N_s_) std::cerr << "\nERROR: i is too large" << std::endl; 			
	
	double v_dot_Omega = v_b_[i] * ( std::cos(theta_v_b_[i]) * std::cos(theta) + std::sin(theta_v_b_[i]) * std::sin(theta) * std::cos(chi - chi_v_b_[i]));

	return nu_0_ * v_dot_Omega / (c_ * Doppler_width_[i]);
}


std::vector<std::complex<double> > RT_problem::compute_T_KQ(const size_t stokes_i, const double theta, const double chi){

	// using standard ordering from tables: T_KQ = [T_00 T_10 T_11 T_20 T_21 T_22] 
	// for Q negative T_K-Q = (-1)^Q * bar(T_KQ)

	// output
	std::vector<std::complex<double> > T_KQ(6, 0.0);

	std::complex<double> eix  = std::polar(1.0, chi);        // exp(i * chi)
	std::complex<double> e2ix = std::polar(1.0, 2.0 * chi);  // exp(2 * i * chi)

	// various coeffs
	complex<double> fa, fb;

	double c2g = std::cos(2.0 * gamma_);
    double ct  = std::cos(theta);
    double ct2 = std::cos(theta) * std::cos(theta);
    double st  = std::sin(theta);    
    double s2g = std::sin(2.0 * gamma_);
    double st2 = std::sin(theta) * std::sin(theta);

    double k  = 1.0 / (2.0 * std::sqrt(2.0));
    double k2 = 0.5 * std::sqrt(3.0);

	if (stokes_i == 0)
	{
		T_KQ[0] = std::real(1.0); 
		
 		T_KQ[3] = std::real(k * (3.0 * ct2 - 1.0));
		T_KQ[4] = - k2 * st * ct * eix;
		T_KQ[5] =  0.5 * k2 * st2 * e2ix;
	}
	else if (stokes_i == 1)
	{
		T_KQ[3] = std::real(- 3.0 * k * c2g * st2);

	  	fa.real(c2g * ct);
    	fa.imag(s2g);
    	fb = st * eix;	

		T_KQ[4] = - k2 * fa * fb;

		fa.real(c2g * (1.0 + ct2));
    	fa.imag(2.0 * s2g * ct);

		T_KQ[5] = - 0.5 * k2 * fa * e2ix;
	}
	else if (stokes_i == 2)
	{
		T_KQ[3] = std::real(3.0 * k * s2g * st2);

		fa.real(s2g * ct);
    	fa.imag(-c2g);

		T_KQ[4] = k2 * fa * st * eix;

		fa.real(s2g * (1.0 + ct2));
    	fa.imag(-2.0 * c2g * ct);

		T_KQ[5] = 0.5 * k2 * fa * e2ix;
	}
	else if (stokes_i == 3)
	{
		T_KQ[1] = std::real(sqrt(3.0 / 2.0) * ct);
		T_KQ[2] = - k2 * st * eix;
	}
	else
	{
		std::cerr << "\nERROR: stokes_i is too large" << std::endl; 			
	}
		
	return T_KQ;
}


std::complex<double> RT_problem::get_TKQi(const size_t i_stokes, const int K, const int Q, const size_t j, const size_t k){

	// checks 	
	if ( i_stokes >= 4) std::cerr  << "\nERROR: i_stokes is too large" << std::endl; 					
	if (K > 2) std::cerr           << "\nERROR: K is too large"        << std::endl; 			
	if (std::abs(Q) > K) std::cerr << "\nERROR: Q is too large"        << std::endl; 	
	if (j >= N_theta_) std::cerr   << "\nERROR: N_theta_ is too large" << std::endl; 	
	if (k >= N_chi_) std::cerr     << "\nERROR: N_chi_ is too large"   << std::endl; 	

	int index = i_stokes * N_theta_ * N_chi_ + j * N_chi_ + k;

	std::complex<double> T_KQ;

	if (K == 0)
	{
		T_KQ = T_KQ_[index][0];
	}
	else if (K == 1)
	{
		if (Q == 0)
		{
			T_KQ = T_KQ_[index][1];
		}
		else if (Q == 1)
		{
			T_KQ = T_KQ_[index][2];
		}
		else if (Q == -1)
		{
			T_KQ = - 1.0 * std::conj(T_KQ_[index][2]);
		}				
		else { std::cerr << "\nERROR: wrong Q input" << std::endl; }		
	}
	else if (K == 2)
	{
		if (Q == 0)
		{
			T_KQ = T_KQ_[index][3];
		}
		else if (Q == 1)
		{
			T_KQ = T_KQ_[index][4];
		}
		else if (Q == -1)
		{
			T_KQ = - 1.0 * std::conj(T_KQ_[index][4]);
		}
		else if (Q == 2)
		{
			T_KQ = T_KQ_[index][5];
		}
		else if (Q == -2)
		{
			T_KQ = std::conj(T_KQ_[index][5]);
		}
		else { std::cerr << "\nERROR: wrong Q input" << std::endl; }		
	}
	else
	{
		std::cerr << "\nERROR: wrong K input" << std::endl; 
	}

	return T_KQ;
}


std::vector<double> RT_problem::compute_eta_and_rhos(const size_t i, const size_t j, const size_t k, const size_t n) {
	
	// checks 		 		
	if (i >= N_s_)     std::cerr << "\nERROR: i is too large" << std::endl; 					
	if (j >= N_theta_) std::cerr << "\nERROR: j is too large" << std::endl; 			
	if (k >= N_chi_)   std::cerr << "\nERROR: k is too large" << std::endl; 	
	if (n >= N_nu_)    std::cerr << "\nERROR: n is too large" << std::endl; 	

	std::vector<double> etas_and_rhos(8, 0.0);

	Rotation_matrix R(0.0, -theta_B_[i], -chi_B_[i]);

	const double theta = theta_grid_[j];
	const double chi   = chi_grid_[k];

	const double coeff  = k_L_[i] / (std::sqrt(pi_) * Doppler_width_[i]);
	const double coeff2 = nu_L_[i] / Doppler_width_[i];

	const std::complex<double> a_damp(0.0, a_[i]);

	// for reduced frequency
	const int index_s_nu = i * N_nu_ + n;
	const double u_red   = u_[index_s_nu] + compute_u_b(i, theta, chi);

	for (int K = 0; K < 3; ++K) 
	{		
		const double coeff_K = coeff * std::sqrt(3.0 * (2.0 * double(K) + 1.0));

		for (int Mu2 = -Ju2_; Mu2 < Ju2_ + 1; Mu2 += 2) 
		{
		 	for (int Ml2 = -Jl2_; Ml2 < Jl2_ + 1; Ml2 += 2) 
		 	{		    
		    	if (std::abs(Mu2 - Ml2) <= 2) 
		    	{		    
		      		const int q2 = Ml2 - Mu2;

		      		const double W3J1 = W3JS(Ju2_, Jl2_, 2,-Mu2, Ml2, -q2);  
		      		const double W3J2 = W3JS(2, 2, 2 * K, q2, -q2, 0); 

		      		const double fact = coeff_K * std::pow(-1.0, double(q2) / 2.0 + 1.0) * std::pow(W3J1, 2) * W3J2;

		      		const double um = coeff2 * (this->gu_ * (double(Mu2) / 2.0) - this->gl_ * (double(Ml2) / 2.0)) + u_red;

		      		for (int Q = -K; Q < K + 1; ++Q) 
		      		{
				        const std::complex<double> faddeva = Faddeeva::w(um + a_damp);
				        const auto D_KQQ                   = std::conj(R(K, 0, Q));

				        const auto fact_re = fact * std::real(faddeva) * D_KQQ;
				        const auto fact_im = fact * std::imag(faddeva) * D_KQQ;

				        // etas
				        etas_and_rhos[0] += std::real(fact_re * get_TKQi(0, K, Q, j, k));
				        etas_and_rhos[1] += std::real(fact_re * get_TKQi(1, K, Q, j, k));
				        etas_and_rhos[2] += std::real(fact_re * get_TKQi(2, K, Q, j, k));
				        etas_and_rhos[3] += std::real(fact_re * get_TKQi(3, K, Q, j, k));

				        // rhos
				        etas_and_rhos[5] += std::real(fact_im * get_TKQi(1, K, Q, j, k));
				        etas_and_rhos[6] += std::real(fact_im * get_TKQi(2, K, Q, j, k));
				        etas_and_rhos[7] += std::real(fact_im * get_TKQi(3, K, Q, j, k));
			        }
		    	}
		  	}
		}
	}

	if (enable_continuum_)
	etas_and_rhos[0] += k_c_[index_s_nu];

	// if (j == 1 and k == 0 and n == 0) cout << etas_and_rhos[0] << endl;
	// if (j == 1 and k == 0 and n == 0) cout << "u_[index_s_nu] = " << u_[index_s_nu] << endl;

	return etas_and_rhos;
}


void const RT_problem::print_info(){
	
	if (mpi_rank_ == 0) 		
	{		
		std::cout << "\n=========== 2-levels atom parameters ===========\n" << std::endl;		
		std::cout << "Mass = " << mass_ << std::endl;
		std::cout << "El = "   << El_   << std::endl;
		std::cout << "Eu = "   << Eu_   << std::endl;		
		std::cout << "2Jl = "  << Jl2_  << std::endl;
		std::cout << "2Ju = "  << Ju2_  << std::endl;		
		std::cout << "gl = "   << gl_   << std::endl;		
		std::cout << "gu = "   << gu_   << std::endl;
		std::cout << "Aul = "  << Aul_  << std::endl;
		
		std::cout << "\n=========== Grids parameters ===========\n" << std::endl;	
		std::cout << "N_s = "     << N_s_     << std::endl;	
		std::cout << "N_theta = " << N_theta_ << std::endl;	
		std::cout << "N_chi = "   << N_chi_   << std::endl;	
		std::cout << "N_nu = "    << N_nu_    << std::endl;			
		
		std::cout << "\ntotal size = " << tot_size_   << std::endl;			
		std::cout << "block size = "   << block_size_ << std::endl;			

		std::cout << "\ntheta grid = [ ";

		for (int i = 0; i < (int)N_theta_; ++i) std::cout << theta_grid_[i] << " ";

		std::cout << "]\nmu grid    = [ ";

		for (int i = 0; i < (int)N_theta_; ++i) std::cout   << mu_grid_[i] << " ";

		std::cout << "]\nchi grid   = [ ";

		for (int i = 0; i < (int)N_chi_; ++i) std::cout   << chi_grid_[i] << " ";

		std::cout << "] " << std::endl;	
	}
}


void const RT_problem::check_sizes(){
	
    if (mpi_rank_ == 0) 		
	{		
		if (Nl_.size() != N_s_) std::cerr   << "\nERROR: Nl_ size is not correct!" << std::endl; 			
		if (T_.size() != N_s_) std::cerr    << "\nERROR: T_ size is not correct!"  << std::endl; 			
		if (xi_.size() != N_s_) std::cerr   << "\nERROR: xi_ size is not correct!" << std::endl; 			
		if (nu_L_.size() != N_s_) std::cerr << "\nERROR: nu_L_ size is not correct!" << std::endl; 
		if (Cul_.size() != N_s_) std::cerr  << "\nERROR: Cul_ size is not correct!" << std::endl; 			
		if (a_.size() != N_s_) std::cerr    << "\nERROR: a_ size is not correct!" << std::endl; 			
		if (W_T_.size() != N_s_) std::cerr  << "\nERROR: W_T_ size is not correct!" << std::endl; 			
		if (Doppler_width_.size() != N_s_) std::cerr << "\nERROR: Doppler_width_ size is not correct!" << std::endl; 	
		if (k_L_.size() != N_s_) std::cerr     << "\nERROR: k_L_ size is not correct!" << std::endl;
		if (epsilon_.size() != N_s_) std::cerr << "\nERROR: k_L_ size is not correct!" << std::endl;
		if (v_b_.size() != N_s_) std::cerr     << "\nERROR: v_b_ size is not correct!" << std::endl;
		if (theta_v_b_.size() != N_s_) std::cerr << "\nERROR: theta_v_b_ size is not correct!" << std::endl;
		if (chi_v_b_.size() != N_s_) std::cerr   << "\nERROR: chi_v_b_ size is not correct!" << std::endl;
		if (D1_.size() != N_s_) std::cerr   << "\nERROR: D1_ size is not correct!" << std::endl;
		if (D2_.size() != N_s_) std::cerr   << "\nERROR: D2_ size is not correct!" << std::endl;

		if (enable_continuum_)
		{
			if (sigma_.size() != N_s_ * N_nu_) std::cerr    << "\nERROR: sigma_ size is not correct!" << std::endl;
			if (k_c_.size() != N_s_ * N_nu_) std::cerr      << "\nERROR: k_c_ size is not correct!" << std::endl;
			if (eps_c_th_.size() != N_s_ * N_nu_) std::cerr << "\nERROR: eps_c_th_ size is not correct!" << std::endl;	
		}
		
		if (u_.size() != N_s_ * N_nu_) std::cerr << "\nERROR: u_ size is not correct!" << std::endl;

		if (T_KQ_.size() != 4 * N_theta_ * N_chi_) std::cerr << "\nERROR: T_KQ_ size is not correct!" << std::endl;
	}			

	// TODO add all vectors
}


void const RT_problem::print_TKQ(){

	// print TKQ
    for (int i = 0; i < 4; ++i)
    {        
        for (int K = 0; K < 3; ++K)
        {
            for (int Q = -K; Q < K + 1; ++Q)
            {
                std::cout << "\n--- Stokes = " << i;
                std::cout << ", K = " << K;
                std::cout << ", Q = " << Q << " --- "<< std::endl;

                std::cout << get_TKQi(i, K, Q, 0, 0) << std::endl;
                std::cout << get_TKQi(i, K, Q, 0, 1) << std::endl;
                std::cout << get_TKQi(i, K, Q, 1, 0) << std::endl;    
                std::cout << get_TKQi(i, K, Q, 1, 1) << std::endl;      

                std::cout << "--------------------------------" << std::endl;                      
            }
        }
    } 
}


void const RT_problem::print_profile(const Vec &field, const int i_stoke, const int j, const int k){

	PetscErrorCode ierr;

	Vec I_reduced;

	ierr = VecCreate(PETSC_COMM_WORLD, &I_reduced);CHKERRV(ierr);		
	ierr = VecSetSizes(I_reduced,PETSC_DECIDE, N_s_ * N_nu_);CHKERRV(ierr);		
	ierr = VecSetBlockSize(I_reduced,N_nu_);CHKERRV(ierr);
	ierr = VecSetFromOptions(I_reduced);CHKERRV(ierr);

	int istart, iend;
	
	ierr = VecGetOwnershipRange(I_reduced, &istart, &iend);CHKERRV(ierr);

	int n, i_space, global_index;

	double value;

	for (int i = istart; i < iend; ++i)
	{	
		i_space = i/N_nu_;
		n = i % N_nu_;

		global_index = local_to_global(i_space, j, k, n);
		global_index += i_stoke;

		ierr = VecGetValues(field, 1, &global_index, &value);CHKERRV(ierr);	

		ierr = VecSetValue(I_reduced, i, value, INSERT_VALUES);CHKERRV(ierr);	
	}	

	ierr = VecAssemblyBegin(I_reduced);CHKERRV(ierr); 
	ierr = VecAssemblyEnd(I_reduced);CHKERRV(ierr); 

	ierr = VecView(I_reduced,PETSC_VIEWER_STDOUT_WORLD);

	ierr = VecDestroy(&I_reduced);CHKERRV(ierr); 		
}

void const RT_problem::print_profile(const Vec &field, const int i_stoke, const int i, const int j, const int k){

	PetscErrorCode ierr;

	double value;
	
	MPI_Barrier(MPI_COMM_WORLD);

	const int start_index = local_to_global(i, j, k, 0) + i_stoke;							
	const int end_index   = start_index + 4 * N_nu_;

	int start, end;
    ierr = VecGetOwnershipRange(field, &start, &end);CHKERRV(ierr);  

	if (start_index >= start and end_index <= end)
	{
		switch (i_stoke)
		{
			case (0): 
				std::cout << "Surface radiation, Stoke parameter I, ";
				break;
			case (1): 
				std::cout << "Surface radiation, Stoke parameter Q, ";
				break;
			case (2): 
				std::cout << "Surface radiation, Stoke parameter U, ";
				break;
			case (3): 
				std::cout << "Surface radiation, Stoke parameter V, ";
				break;
			default:
				std::cout << "ERROR: i_stoke should be smaller then 4!" << std::endl;
		}

		std::cout << "h =  " << depth_grid_[i] << " km , " << "mu =  "  
				  << mu_grid_[j] << ", chi =  " << chi_grid_[k] << std::endl;			

		for (int i = start_index; i < end_index; i = i + 4)
		{
			ierr = VecGetValues(field, 1, &i, &value);CHKERRV(ierr);
			
			std::cout << value << std::endl;
		}
    }
	
	MPI_Barrier(MPI_COMM_WORLD);
}

void const RT_problem::print_surface_profile(const Vec &field, const int i_stoke, const int j, const int k){

	PetscErrorCode ierr;

	double value;
	int start_index, end_index;

	MPI_Barrier(MPI_COMM_WORLD);

	if (mpi_rank_ == 0)
	{
		switch (i_stoke)
		{
			case (0): 
				std::cout << "Surface radiation, Stoke parameter I, ";
				break;
			case (1): 
				std::cout << "Surface radiation, Stoke parameter Q, ";
				break;
			case (2): 
				std::cout << "Surface radiation, Stoke parameter U, ";
				break;
			case (3): 
				std::cout << "Surface radiation, Stoke parameter V, ";
				break;
			default:
				std::cout << "ERROR: i_stoke should be smaller then 4!" << std::endl;
		}

		if (j >= 0)
		{	
			std::cout << "mu =  "  << mu_grid_[j];
			std::cout << ", chi =  " << chi_grid_[k] << std::endl;

			start_index = 4 * N_nu_ * (k + j * N_chi_) + i_stoke; 
		}
		else // print last part of first block (default)
		{			
			std::cout << "mu =  "  << mu_grid_[N_theta_ - 1];
			std::cout << ", chi =  " << chi_grid_[N_chi_ - 1]  << std::endl;

			start_index =  block_size_ - 4 * N_nu_ + i_stoke;						
		}

		end_index = start_index + 4 * N_nu_;

		for (int i = start_index; i < end_index; i = i + 4)
		{
			ierr = VecGetValues(field, 1, &i, &value);CHKERRV(ierr);
			
			std::cout << value << std::endl;
		}
	}

	MPI_Barrier(MPI_COMM_WORLD);
}






