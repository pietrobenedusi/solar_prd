#ifndef RT_problem_hpp
#define RT_problem_hpp

#include "Utilities.hpp"
#include "Rotation_matrix.hpp"

class RT_problem
{

public:

	// constructor
	RT_problem(const std::string input_path, const size_t N_theta, const size_t N_chi)			   
	{
		// assign MPI varaibles 
    	MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank_);
    	MPI_Comm_size(MPI_COMM_WORLD, &mpi_size_);

    	if (mpi_rank_ == 0) std::cout << "\n~~~~~~ MPI size = " << mpi_size_ << " ~~~~~~" << std::endl;
		if (mpi_rank_ == 0) std::cout << "\n=========== Reading input files ===========\n" << std::endl;				

    	//reading input
    	read_atom(           input_path + "/atom.dat");
		read_atmosphere(     input_path + "/atmosphere.dat");
		read_bulk_velocity(  input_path + "/bulk_velocity.dat");
		read_frequency(      input_path + "/frequency.dat");		
		read_magnetic_field( input_path + "/magnetic_field.dat");

		// reading continuum
		read_sigma(    input_path + "/continuum/continuum_scat_opac.dat");
		read_k_c(      input_path + "/continuum/continuum_tot_opac.dat");
		read_eps_c_th( input_path + "/continuum/continuum_therm_emiss.dat");			

		// refine if necessary	
		// refine_frequency();	
		// refine_atmosphere();
		// refine_atmosphere();
		// refine_atmosphere();
		
		// set sizes and directions grids and weigths
		set_theta_chi_grids(N_theta, N_chi);
		set_sizes();		

		// print
		print_info();	

		// init I, S, eta_field and rho_field to zero and set BCs // TODO choose better initialization
		init_fields();

		// precompute what needed
		set_up();

		// save_vec(eta_field_, "../output/eta_.m" ,"eta");      

		// safety test
		check_sizes(); 		
	}

	// grids
	std::vector<double> depth_grid_; // in Km 
	std::vector<double> nu_grid_; 
	std::vector<double> theta_grid_; 
	std::vector<double> mu_grid_; 	
	std::vector<double> chi_grid_; 

	// grids sizes
	size_t N_s_;     // space (index i)	
	size_t N_theta_; // inclination (index j)
	size_t N_chi_;   // azimuth (index k)
	size_t N_nu_;    // frequency (index n)
	
	size_t block_size_; // 4 * N_nu_ * N_theta_ * N_chi_;
	size_t tot_size_;   // N_s_ * block_size;
	
	// unknown quantities 
	Vec I_field_; // intesity 
	Vec S_field_; // source function
	
	// propagation matrix entries 
	Vec eta_field_; 
	Vec rho_field_;
	
	// optical depth steps (dt_i corresponds to [x_i x_i+1])
	Vec dtau_;	

	// getters
	std::vector<double> get_field(const size_t i, const size_t j, const size_t k, const size_t n, const Vec &field);
	double       get_field_scalar(const size_t i, const size_t j, const size_t k, const size_t n, const Vec &field);

	
	// convert local indeces to global one (of fields) for the first Stokes parameter and vice versa
	inline size_t local_to_global(const size_t i, const size_t j, const size_t k, const size_t n)
	{
		// block_size_ * i + 4 * ( N_chi_ * N_nu_ * j + N_nu_ * k + n);
		return block_size_ * i + 4 * ( N_nu_ * ( N_chi_ * j + k ) + n);
	}

	// convert global index to local ones = [i, j, k, nu, i_stokes]
	inline std::vector<size_t> local_to_global(const size_t i_global)
	{
		std::vector<size_t> local_indeces;

		const size_t i_stokes =  i_global % 4;
		const size_t n        = (i_global / 4) % N_nu_;
		const size_t k        = (i_global / (4 * N_nu_)) % N_chi_;
		const size_t j        = (i_global / (4 * N_nu_ * N_chi_)) % N_theta_;
		const size_t i        = (i_global / (4 * N_nu_ * N_chi_ * N_theta_));

		local_indeces.push_back(i);
		local_indeces.push_back(j);
		local_indeces.push_back(k);
		local_indeces.push_back(n);
		local_indeces.push_back(i_stokes);

		return local_indeces;
	}
				
	// MPI varables
	int mpi_rank_;
	int mpi_size_;

	// flag to enable continuum 
	bool enable_continuum_ = true;

	// physical and math constants 
	const double c_   = 2.99792458e+10;
	const double k_B_ = 1.38065e-16;
	const double h_   = 6.62607e-27;
	const double pi_  = 3.1415926535897932384626;	  

	// 2-level atom constants
	double mass_;
	double El_;
	double Eu_;
	int Jl2_;
	int Ju2_;
	double gl_;
	double gu_;
	double Aul_;	// Einstein coefficients for spontaneous emission

	// reference frame
	double gamma_ = 0.5 * pi_;	  

	// atom constant, to precompute
	double nu_0_;	

	// depolarizing rate due to elastic collisions
	std::vector<double> D1_, D2_; 
		
	// Legendre and trapezoidal weights 
	std::vector<double> w_theta_;
	std::vector<double> w_chi_;

	// input quantities depending on position 
	std::vector<double> Nl_;   // lower level populations 
	std::vector<double> Nu_;   // upper level populations 
	std::vector<double> T_;    // temperature 
	std::vector<double> xi_;   // microturbulent velocity (a.k.a. non-thermal microscopic velocity)		
	std::vector<double> nu_L_; // Larmor frequency
	std::vector<double> Cul_;  // rate of inelastic de-exciting collisions
	std::vector<double> Qel_;  // rate of elastic collisions 
	std::vector<double> a_;    // damping constant 
	std::vector<double> W_T_;  // Wien function

	// magnetic field direction, in polar cooridantes   
	std::vector<double> theta_B_; 
	std::vector<double> chi_B_;   

	// bulk velocities, in polar coordinates
	std::vector<double> v_b_;       
	std::vector<double> theta_v_b_; 
	std::vector<double> chi_v_b_;   
	
	// quantities depending on position that can be precomputed
	std::vector<double> Doppler_width_;
	std::vector<double> k_L_;      // frequency-integrated absorption coefficient
	std::vector<double> epsilon_;  // thermalization parameter

	// input quantities depending on position and frequency // TODO -----------> Vec
	std::vector<double> u_;      // reduced frequencies 
	std::vector<double> sigma_;  // continuum coeffs
	std::vector<double> k_c_;
	std::vector<double> eps_c_th_;

	// quantities depending on position and direction
	std::vector<std::vector<std::complex<double> > > T_KQ_; // polarization tensor 

	// print TKQ tensors (for debug purposeses)
	void const print_TKQ();

	// print outgoing surface profile 
	void const print_profile(const Vec &field, const int i_stoke, const int j, const int k);
	void const print_profile(const Vec &field, const int i_stoke, const int i, const int j, const int k);
	void const print_surface_profile(const Vec &field, const int i_stoke = 0, const int j = -1, const int k = 0);

	
	private:

	// compute Doppler shifts [TODO: precompute]
	double compute_u_b(size_t i, double theta, double chi);

	// polarization tensor in the “vertical” reference system
	std::vector<std::complex<double> > compute_T_KQ(const size_t stokes_i, const double theta, const double chi);
	std::complex<double> get_TKQi(const size_t i_stokes, const int K, const int Q, const size_t j, const size_t k);

	// compute elements of the propagation matrix K
	std::vector<double> compute_eta_and_rhos(const size_t i, const size_t j, const size_t k, const size_t n);

	// ------------------ misc. ------------------ //

	// read inputs
	void read_atom(std::string filename);
	void read_atmosphere(std::string filename);
	void read_bulk_velocity(std::string filename);
	void read_frequency(std::string filename);
	void read_magnetic_field(std::string filename);

	// read continuum
	void read_sigma(std::string filename);
	void read_k_c(std::string filename);
	void read_eps_c_th(std::string filename);			   

	// set grids and sizes
	void set_theta_chi_grids(const size_t N_theta, const size_t N_chi, const bool double_GL = true);
	void set_sizes();

	// refine atmospheric input
	void refine_atmosphere();

	// refine frequency grid in input
	void refine_frequency();
		
	// precompute quantities
	void set_up();

	// init I,S, eta and rho using the following order: [i j k n i_stokes]
	void init_fields();

	// perform checks
	void const check_sizes();	

	// print infos on screen
	void const print_info();		
};

#endif 
