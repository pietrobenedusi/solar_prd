#include "RT_solver.hpp"

void MF_context::formal_solve_single_ray(Vec &I_field, const Vec &S_field, const size_t j, const size_t k, const size_t n){

	PetscErrorCode ierr;
			
	const auto block_size = RT_problem_->block_size_;

	const auto dtau_field = &RT_problem_->dtau_;
	const auto eta_field  = &RT_problem_->eta_field_;
	const auto rho_field  = &RT_problem_->rho_field_;

	const auto mu_grid = &RT_problem_->mu_grid_;
		
  	int ix[4];
  	int istart, iend; 
	ierr = VecGetOwnershipRange(I_field, &istart, &iend);CHKERRV(ierr);	
	size_t iglobal;

	const int istart_local = istart / block_size;
	const int iend_local   = iend   / block_size;

	double dtau, mu, mu_coeff;
	bool ingoing_ray;

	// initial condition
	std::vector<double> I0(4);

	std::vector<double> I1(4), I2(4), S1(4), S2(4), etas(4), rhos(4), K1(16), K2(16);

	mu = (*mu_grid)[j];  
	ingoing_ray = mu < 0; 

	mu_coeff = - 1.0 / std::abs(mu); // minus for optical depth conversion
			
	if (ingoing_ray)
	{							
		for (int i = istart_local; i < iend_local - 1; ++i)
		{	
			// set global index
			iglobal = RT_problem_->local_to_global(i, j, k, n);

			std::iota(ix, ix + 4, iglobal); 

			// receive init condition			
			if (i == istart_local and mpi_rank_ > 0)
			{					
				MPI_Recv(&I0[0], 4, MPI_DOUBLE, mpi_rank_ - 1, iglobal - block_size, MPI_COMM_WORLD, MPI_STATUS_IGNORE); // TODO? S2 K2?

				// set initial condition of current slab
				ierr = VecSetValues(I_field, 4, ix, &I0[0], INSERT_VALUES);CHKERRV(ierr); 							
			}
								
			// get I_field						
			ierr = VecGetValues(I_field, 4, ix, &I1[0]);CHKERRV(ierr);							
			ierr = VecGetValues(S_field, 4, ix, &S1[0]);CHKERRV(ierr);	

			dtau = mu_coeff * (RT_problem_->get_field_scalar(i, j, k, n, *dtau_field)); 

			etas = RT_problem_->get_field(i, j, k, n, *eta_field);
			rhos = RT_problem_->get_field(i, j, k, n, *rho_field);

			K1 = assemble_propagation_matrix_scaled(etas, rhos);

			// for the next point for multistep methods 
			std::iota(ix, ix + 4, iglobal + block_size);

			ierr = VecGetValues(I_field, 4, ix, &I2[0]);CHKERRV(ierr);	
			ierr = VecGetValues(S_field, 4, ix, &S2[0]);CHKERRV(ierr);	

			etas = RT_problem_->get_field(i + 1, j, k, n, *eta_field);
			rhos = RT_problem_->get_field(i + 1, j, k, n, *rho_field);
			
			K2 = assemble_propagation_matrix_scaled(etas, rhos);

			formal_solver_.one_step(dtau, K1, K2, S1, S2, I1, I2);
			
			// insert new value
			ierr = VecSetValues(I_field, 4, ix, &I2[0], INSERT_VALUES);CHKERRV(ierr); 

			// send initial condition to next processor
			if (i == iend_local - 2 and mpi_rank_ < mpi_size_ - 1)
			{																												
				MPI_Send(&I2[0], 4, MPI_DOUBLE, mpi_rank_ + 1, iglobal + block_size, MPI_COMM_WORLD);							
			}
		}		
	}
	else
	{
		for (int i = iend_local - 1; i > istart_local; --i)
		{
			// set global index
			iglobal = RT_problem_->local_to_global(i, j, k, n);

			std::iota(ix, ix + 4, iglobal); 

			// receive init condition			
			if (i == iend_local - 1 and mpi_rank_ < mpi_size_ - 1)
			{													
				MPI_Recv(&I0[0], 4, MPI_DOUBLE, mpi_rank_ + 1, iglobal + block_size, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

				// set initial condition of current slab
				ierr = VecSetValues(I_field, 4, ix, &I0[0], INSERT_VALUES);CHKERRV(ierr); 							
			} 
			
			// get I_field						
			ierr = VecGetValues(I_field, 4, ix, &I1[0]);CHKERRV(ierr);							
			ierr = VecGetValues(S_field, 4, ix, &S1[0]);CHKERRV(ierr);	

			dtau = mu_coeff * (RT_problem_->get_field_scalar(i - 1, j, k, n, *dtau_field)); 

			etas = RT_problem_->get_field(i, j, k, n, *eta_field);
			rhos = RT_problem_->get_field(i, j, k, n, *rho_field);

			K1 = assemble_propagation_matrix_scaled(etas, rhos);

			// for the next point for multistep methods 
			std::iota(ix, ix + 4, iglobal - block_size);

			ierr = VecGetValues(I_field, 4, ix, &I2[0]);CHKERRV(ierr);	
			ierr = VecGetValues(S_field, 4, ix, &S2[0]);CHKERRV(ierr);	

			etas = RT_problem_->get_field(i - 1, j, k, n, *eta_field);
			rhos = RT_problem_->get_field(i - 1, j, k, n, *rho_field);
			
			K2 = assemble_propagation_matrix_scaled(etas, rhos);

			formal_solver_.one_step(dtau, K1, K2, S1, S2, I1, I2);

			// insert new value
			ierr = VecSetValues(I_field, 4, ix, &I2[0], INSERT_VALUES);CHKERRV(ierr); 
			
			// send initial condition to next processor
			if (i == istart_local + 1 and mpi_rank_ > 0)
			{																	
				MPI_Send(&I2[0], 4, MPI_DOUBLE, mpi_rank_ - 1, iglobal - block_size, MPI_COMM_WORLD);							
			}
		}				
	}
}


void MF_context::formal_solve_threaded(Vec &I_field, const Vec &S_field, const double I0){

	PetscErrorCode ierr;

	if (mpi_rank_ == 0) std::cout << "\nStart formal solution (multi threaded)...\n"<< std::endl;

	const auto N_theta = RT_problem_->N_theta_;
	const auto N_chi   = RT_problem_->N_chi_;
	const auto N_nu    = RT_problem_->N_nu_;

	const auto block_size = RT_problem_->block_size_;

	std::vector<thread> threads;
	threads.reserve(N_theta * N_chi * N_nu);

	int istart, iend; 
	ierr = VecGetOwnershipRange(I_field, &istart, &iend);CHKERRV(ierr);	
	
	// set BC on I_field (only for the first Stokes parameter)	
	for (int i = istart; i < (int) block_size; i = i + 4)
	{
		ierr = VecSetValue(I_field, i, I0, INSERT_VALUES);CHKERRV(ierr);					
	}
	
	// loop over directions and frequencies 
	for (size_t j = 0; j < N_theta; ++j)     
	{
		for (size_t k = 0; k < N_chi; ++k)
		{
			for (size_t n = 0; n < N_nu; ++n)
			{
				threads.push_back(thread(&MF_context::formal_solve_single_ray, *this, std::ref(I_field), std::ref(S_field), j, k ,n));				
			}
		}
	}

	for (auto& th : threads) 
	{
        th.join();
    }	

    ierr = VecAssemblyBegin(I_field);CHKERRV(ierr); 
  	ierr = VecAssemblyEnd(I_field);CHKERRV(ierr); 
}


// TODO: improve communication pattern
void MF_context::formal_solve(Vec &I_field, const Vec &S_field, const double I0){

	if (mpi_rank_ == 0) std::cout << "\nStart formal solution (single threaded)..." << std::endl;

	PetscErrorCode ierr;
	
	const auto N_theta = RT_problem_->N_theta_;
	const auto N_chi   = RT_problem_->N_chi_;
	const auto N_nu    = RT_problem_->N_nu_;
	const auto N_s     = RT_problem_->N_s_;

	const auto block_size = RT_problem_->block_size_;
	const auto tot_size   = RT_problem_->tot_size_;
	
	const auto dtau_field = &RT_problem_->dtau_;
	const auto eta_field  = &RT_problem_->eta_field_;
	const auto rho_field  = &RT_problem_->rho_field_;

	const auto mu_grid = &RT_problem_->mu_grid_;
		
  	int ix[4], ix_pre[4];
  	int istart, iend; 
	ierr = VecGetOwnershipRange(I_field, &istart, &iend);CHKERRV(ierr);	
	size_t iglobal, tag;	

	const int istart_local = istart / block_size;
	const int iend_local   = iend   / block_size;

	double dtau, mu, mu_coeff;
	bool ingoing_ray;

	// quantiteis depending on spatial point i
	std::vector<double> I1(4), I2(4), S1(4), S2(4), etas(4), rhos(4), K1(16), K2(16);	

	// test
	// dtau = - 0.1;
	// std::vector<double> K1{ 1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0 };
	// std::vector<double> K2{ 1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0 };

	// set BC on I_field (only for the first Stokes parameter)	
	const double value = I0 * (RT_problem_->W_T_[N_s - 1]);

	for (int i = istart; i < iend; i = i + 4)
	{
		// scalar BC: I0 * W_T_[end]				
		if (i >= (int) (tot_size - block_size)) ierr = VecSetValue(I_field, i, value, INSERT_VALUES);CHKERRV(ierr);																	
	}
		
	// loop over directions and frequencies 
	for (size_t j = 0; j < N_theta; ++j)    
	{		
		mu = (*mu_grid)[j]; 
		ingoing_ray = mu < 0; 

		mu_coeff = - 1.0 / std::abs(mu); // minus for optical depth conversion
				
		for (size_t k = 0; k < N_chi; ++k)
		{		
			for (size_t n = 0; n < N_nu; ++n)
			{
				tag = N_nu * ( N_chi * j + k ) + n;

				if (ingoing_ray) // from surface to deep
				{							
					for (int i = istart_local; i < iend_local; ++i)
					{							
						// set global index
						iglobal = RT_problem_->local_to_global(i, j, k, n);

						// assemble K2
						etas = RT_problem_->get_field(i, j, k, n, *eta_field);
						rhos = RT_problem_->get_field(i, j, k, n, *rho_field);
						
						K2 = assemble_propagation_matrix_scaled(etas, rhos);

						// set S2
						std::iota(ix, ix + 4, iglobal); 

						ierr = VecGetValues(S_field, 4, ix, &S2[0]);CHKERRV(ierr);									
																														
						if (i == istart_local and mpi_rank_ == 0) // initial condition
						{
							// fill I2 to send in case only one proc for space point is used
							if (i == iend_local - 1 and mpi_rank_ < mpi_size_ - 1) ierr = VecGetValues(I_field, 4, ix, &I2[0]);CHKERRV(ierr);																								
						}
						else if (i == istart_local and mpi_rank_ > 0) // first node from local processor
						{	 
							// get initial condition, K and dtau from previous proc						
							MPI_CHECK(MPI_Recv(&I1[0],  4, MPI_DOUBLE, mpi_rank_ - 1, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE));							
							MPI_CHECK(MPI_Recv(&K1[0], 16, MPI_DOUBLE, mpi_rank_ - 1, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE));
							MPI_CHECK(MPI_Recv(&S1[0],  4, MPI_DOUBLE, mpi_rank_ - 1, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE));
							MPI_CHECK(MPI_Recv(&dtau,   1, MPI_DOUBLE, mpi_rank_ - 1, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE));
																												
							// perform formal solve
							formal_solver_.one_step(dtau, K1, K2, S1, S2, I1, I2);

							// set initial condition of current slab with
							ierr = VecSetValues(I_field, 4, ix, &I2[0], INSERT_VALUES);CHKERRV(ierr); 							
						}
						else // interior point
						{													
							std::iota(ix_pre, ix_pre + 4, iglobal - block_size); 

							// get I_field						
							ierr = VecGetValues(I_field, 4, ix_pre, &I1[0]);CHKERRV(ierr);							
							ierr = VecGetValues(S_field, 4, ix_pre, &S1[0]);CHKERRV(ierr);									

							dtau = mu_coeff * (RT_problem_->get_field_scalar(i - 1, j, k, n, *dtau_field)); 								

							etas = RT_problem_->get_field(i - 1, j, k, n, *eta_field);
							rhos = RT_problem_->get_field(i - 1, j, k, n, *rho_field);

							K1 = assemble_propagation_matrix_scaled(etas, rhos);
							
							// for the next point for multistep methods 													
							formal_solver_.one_step(dtau, K1, K2, S1, S2, I1, I2);
											
							// insert new value
							ierr = VecSetValues(I_field, 4, ix, &I2[0], INSERT_VALUES);CHKERRV(ierr); 							
						}						

						// send initial condition to next processor
						if (i == iend_local - 1 and mpi_rank_ < mpi_size_ - 1)
						{	
							dtau = mu_coeff * (RT_problem_->get_field_scalar(i, j, k, n, *dtau_field));							

							// send 			
							MPI_CHECK(MPI_Send(&I2[0],  4, MPI_DOUBLE, mpi_rank_ + 1, tag, MPI_COMM_WORLD));
							MPI_CHECK(MPI_Send(&K2[0], 16, MPI_DOUBLE, mpi_rank_ + 1, tag, MPI_COMM_WORLD));	
							MPI_CHECK(MPI_Send(&S2[0],  4, MPI_DOUBLE, mpi_rank_ + 1, tag, MPI_COMM_WORLD));	
							MPI_CHECK(MPI_Send(&dtau,   1, MPI_DOUBLE, mpi_rank_ + 1, tag, MPI_COMM_WORLD));								
						}
					}		
				}
				else // outgoing ray (from deep to surface)
				{
					for (int i = iend_local - 1; i >= istart_local; --i) 
					{								
						// set global index
						iglobal = RT_problem_->local_to_global(i, j, k, n);	

						// assemble K2
						etas = RT_problem_->get_field(i, j, k, n, *eta_field);
						rhos = RT_problem_->get_field(i, j, k, n, *rho_field);	

						K2 = assemble_propagation_matrix_scaled(etas, rhos);							

						// set S2
						std::iota(ix, ix + 4, iglobal);

						ierr = VecGetValues(S_field, 4, ix, &S2[0]);CHKERRV(ierr);																																		

						// receive init condition		
						if (i == iend_local - 1 and mpi_rank_ == mpi_size_ - 1) // first initial condition
						{
							// fill I2 to send in case only one proc for space point is used
							if (i == istart_local and mpi_rank_ > 0) ierr = VecGetValues(I_field, 4, ix, &I2[0]);CHKERRV(ierr);																							
						}	
						else if (i == iend_local - 1 and mpi_rank_ < mpi_size_ - 1) // initial condition of local processor
						{								
							MPI_CHECK(MPI_Recv(&I1[0],  4, MPI_DOUBLE, mpi_rank_ + 1, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE));
							MPI_CHECK(MPI_Recv(&K1[0], 16, MPI_DOUBLE, mpi_rank_ + 1, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE));
							MPI_CHECK(MPI_Recv(&S1[0],  4, MPI_DOUBLE, mpi_rank_ + 1, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE));							

							dtau = mu_coeff * (RT_problem_->get_field_scalar(i, j, k, n, *dtau_field)); 													
														
							// perform formal solve
							formal_solver_.one_step(dtau, K1, K2, S1, S2, I1, I2);

							// set initial condition of current slab
							ierr = VecSetValues(I_field, 4, ix, &I2[0], INSERT_VALUES);CHKERRV(ierr); 							
						} 
						else // interior point
						{							
							std::iota(ix_pre, ix_pre + 4, iglobal + block_size); 

							// get I_field						
							ierr = VecGetValues(I_field, 4, ix_pre, &I1[0]);CHKERRV(ierr);							
							ierr = VecGetValues(S_field, 4, ix_pre, &S1[0]);CHKERRV(ierr);	

							dtau = mu_coeff * (RT_problem_->get_field_scalar(i, j, k, n, *dtau_field)); 

							etas = RT_problem_->get_field(i + 1, j, k, n, *eta_field);
							rhos = RT_problem_->get_field(i + 1, j, k, n, *rho_field);

							K1 = assemble_propagation_matrix_scaled(etas, rhos);							

							// for the next point for multistep methods 							
							formal_solver_.one_step(dtau, K1, K2, S1, S2, I1, I2);								
							
							// insert new value
							ierr = VecSetValues(I_field, 4, ix, &I2[0], INSERT_VALUES);CHKERRV(ierr);
						}	
						
						// send initial condition to next processor
						if (i == istart_local and mpi_rank_ > 0)
						{										
							MPI_CHECK(MPI_Send(&I2[0],  4, MPI_DOUBLE, mpi_rank_ - 1, tag, MPI_COMM_WORLD));
							MPI_CHECK(MPI_Send(&K2[0], 16, MPI_DOUBLE, mpi_rank_ - 1, tag, MPI_COMM_WORLD));	
							MPI_CHECK(MPI_Send(&S2[0],  4, MPI_DOUBLE, mpi_rank_ - 1, tag, MPI_COMM_WORLD));								
						}															
					}
				}
			}
		}
	}

	// force non-negativity
	// make_intensity_positive(I_field);

	ierr = VecAssemblyBegin(I_field);CHKERRV(ierr); 
  	ierr = VecAssemblyEnd(I_field);CHKERRV(ierr); 
}

void MF_context::make_intensity_positive(Vec &I_field){

	PetscErrorCode ierr;

	int istart, iend;
	ierr = VecGetOwnershipRange(I_field, &istart, &iend);CHKERRV(ierr);

	double value;

	for (int i = istart; i < iend; i = i + 4)
	{
		ierr = VecGetValues(I_field, 1, &i, &value);CHKERRV(ierr);	

		if (value < 0) ierr = VecSetValue(I_field, i, 0.0, INSERT_VALUES);CHKERRV(ierr);		
	}
}


void MF_context::set_up_emission_module(){

	// Build module
    auto fsf_in_sh_ptr =
    rii_include::formal_solver_factory_from_in_struct::make_formal_solver_factory_from_in_struct_shared_ptr();

    // interface
    rii_include::in_RT_problem_interface<RT_problem> RT_interface;
    RT_interface.add_models(RT_problem_, fsf_in_sh_ptr);

    ecc_sh_ptr_ = rii_include::emission_coefficient_computation::make_emission_coefficient_computation_shared_ptr();

	ecc_sh_ptr_->set_threads_number(12);

    bool flag = ecc_sh_ptr_->build_problem(fsf_in_sh_ptr);

    // ecc_sh_ptr_->set_RIII_exact_KKpQ_dir("/scratch/snx3000/sriva/R_III_exact_tests/KKpQ_matrices/CaI_4227/CaI_1_B0_V0_12T_8C_99F_fg_RIII_ex_limit");

    if (not flag) std::cerr << "Error in set_up_emission_module()!" << std::endl;

    epsilon_computation_function_ = ecc_sh_ptr_->make_computation_function(
		{
			rii_include::emission_coefficient_computation::emission_coefficient_components::epsilon_R_II_CONTRIB,
		 	rii_include::emission_coefficient_computation::emission_coefficient_components::epsilon_R_III_GL,
		 	// rii_include::emission_coefficient_computation::emission_coefficient_components::epsilon_R_III_exact_limit,
		 	// rii_include::emission_coefficient_computation::emission_coefficient_components::epsilon_R_III_CRD_limit,
		 	rii_include::emission_coefficient_computation::emission_coefficient_components::epsilon_csc
		},  
		rii_consts::rii_units::kilometer);

    epsilon_computation_function_approx_ = ecc_sh_ptr_->make_computation_function(
	{
		// rii_include::emission_coefficient_computation::emission_coefficient_components::epsilon_R_II,
	 	rii_include::emission_coefficient_computation::emission_coefficient_components::epsilon_R_III_pCRD_limit,
	 	// rii_include::emission_coefficient_computation::emission_coefficient_components::epsilon_csc
	},  
	rii_consts::rii_units::kilometer);

    // for Luca test
	epsilon_computation_function_approx2_ = ecc_sh_ptr_->make_computation_function(
	{
		rii_include::emission_coefficient_computation::emission_coefficient_components::epsilon_R_II,
	 	// rii_include::emission_coefficient_computation::emission_coefficient_components::epsilon_R_III_CRD_limit,
	 	// rii_include::emission_coefficient_computation::emission_coefficient_components::epsilon_csc
	},  
	rii_consts::rii_units::kilometer);
		 
    offset_f_ = rii_include::make_default_offset_function(RT_problem_->N_theta_, RT_problem_->N_chi_, RT_problem_->N_nu_);
}


// emissivity from scattering (line + continuum)
void MF_context::update_emission(const Vec &I_field, const bool approx){ 	
	
	PetscErrorCode ierr; 
		
	// test Simone module
    const auto block_size = RT_problem_->block_size_;     

    std::vector<double>  input(block_size);        
    std::vector<double> output(block_size); 

    double height;
    int ix[block_size];

    int istart, iend; 
    ierr = VecGetOwnershipRange(I_field, &istart, &iend);CHKERRV(ierr);	
	
	const int istart_local = istart / block_size;
	const int iend_local   = iend   / block_size;

    for (int i = istart_local; i < iend_local; ++i)
    {
    	// set indeces
    	std::iota(ix, ix + block_size, i * block_size);

    	// call Simone module
    	height = RT_problem_->depth_grid_[i];

    	// get I field at ith height
    	ierr = VecGetValues(I_field, block_size, ix, &input[0]);CHKERRV(ierr);	

    	// set input field
		ecc_sh_ptr_->update_incoming_field<double>(height, offset_f_, input.data());

    	ecc_sh_ptr_->set_threads_number(1);
    	
    	if (approx)
    	{
    		auto epsilon_grid = epsilon_computation_function_approx_(height);
	    	rii_include::make_indices_convertion_function<double>(epsilon_grid, offset_f_)(output.data());    
    	}
    	else
    	{
    		auto epsilon_grid = epsilon_computation_function_(height);
	    	rii_include::make_indices_convertion_function<double>(epsilon_grid, offset_f_)(output.data());    
    	}
    	
    	// set S_field_ accordingly        	
    	ierr = VecSetValues(RT_problem_->S_field_, block_size, ix, &output[0], INSERT_VALUES);CHKERRV(ierr);		
    }
    
	// test S = I;
	// ierr = VecCopy(I_field, RT_problem_->S_field_);CHKERRV(ierr);		

	// test S = 1
	// ierr = VecSet(RT_problem_->S_field_, 1.0);CHKERRV(ierr);		

	// test S = 0;
	// ierr = VecZeroEntries(RT_problem_->S_field_);CHKERRV(ierr);		

	// // LTE S = W_T;
	// const auto block_size = RT_problem_->block_size_;
 		
	// int istart, iend; 
	// ierr = VecGetOwnershipRange(RT_problem_->I_field_, &istart, &iend);CHKERRV(ierr);	

	// size_t i_space;
	// double value;

	// for (int i = istart; i < iend; ++i)
	// {
	// 	i_space = i / block_size;

	// 	value = RT_problem_->W_T_[i_space];

	// 	ierr = VecSetValues(RT_problem_->S_field_, 1, &i, &value, INSERT_VALUES);CHKERRV(ierr); 							
	// }

	// ierr = VecAssemblyBegin(RT_problem_->S_field_);CHKERRV(ierr); 
 	// ierr = VecAssemblyEnd(RT_problem_->S_field_);CHKERRV(ierr); 

 	// scale emission by eta_I 
	int ixx[4];
	double eta_I, eta_I_inv;	
	double S[4];
		
	for (int i = istart; i < iend; i = i + 4)
	{
		ierr = VecGetValues(RT_problem_->eta_field_, 1, &i, &eta_I);CHKERRV(ierr);	

		eta_I_inv = 1.0 / eta_I;

		std::iota(ixx, ixx + 4, i);

		ierr = VecGetValues(RT_problem_->S_field_, 4, ixx, S);CHKERRV(ierr);	

		S[0] = eta_I_inv * S[0];
		S[1] = eta_I_inv * S[1];
		S[2] = eta_I_inv * S[2];
		S[3] = eta_I_inv * S[3];

		ierr = VecSetValues(RT_problem_->S_field_, 4, ixx, S, INSERT_VALUES);CHKERRV(ierr); 									
	}

	ierr = VecAssemblyBegin(RT_problem_->S_field_);CHKERRV(ierr); 
 	ierr = VecAssemblyEnd(  RT_problem_->S_field_);CHKERRV(ierr);  
}


void MF_context::get_emissivity(const Vec &I_field){

	PetscErrorCode ierr; 
		
	// test Simone module
    const auto block_size = RT_problem_->block_size_;     

    std::vector<double>  input(block_size);        
    std::vector<double> output(block_size); 

    double height;
    int ix[block_size];

    int istart, iend; 
    ierr = VecGetOwnershipRange(I_field, &istart, &iend);CHKERRV(ierr);	
	
	const int istart_local = istart / block_size;
	const int iend_local   = iend   / block_size;

    for (int i = istart_local; i < iend_local; ++i)
    {
    	// set indeces
    	std::iota(ix, ix + block_size, i * block_size);

    	// call Simone module
    	height = RT_problem_->depth_grid_[i];

    	// get I field at ith height
    	ierr = VecGetValues(I_field, block_size, ix, &input[0]);CHKERRV(ierr);	

    	// set input field
		ecc_sh_ptr_->update_incoming_field<double>(height, offset_f_, input.data());

    	ecc_sh_ptr_->set_threads_number(1);
    	    	
    	// auto epsilon_grid = epsilon_computation_function_(height);
	    // auto epsilon_grid = epsilon_computation_function_approx_(height);
	    auto epsilon_grid = epsilon_computation_function_approx2_(height);
	    rii_include::make_indices_convertion_function<double>(epsilon_grid, offset_f_)(output.data());     
    	    	
    	// set S_field_ accordingly        	
    	ierr = VecSetValues(RT_problem_->S_field_, block_size, ix, &output[0], INSERT_VALUES);CHKERRV(ierr);		
    }

	ierr = VecAssemblyBegin(RT_problem_->S_field_);CHKERRV(ierr); 
 	ierr = VecAssemblyEnd(  RT_problem_->S_field_);CHKERRV(ierr);
}

void RT_solver::save_Lamda(){

	if (mpi_size_ > 1) std::cout << "WARNING in save mat!"<< std::endl;

	const bool L_flag = false;

	PetscErrorCode ierr;

	const auto tot_size = RT_problem_->tot_size_;

	Mat L;

	ierr = MatCreate(PETSC_COMM_WORLD, &L);CHKERRV(ierr);
    ierr = MatSetSizes(L,PETSC_DECIDE, PETSC_DECIDE, tot_size, tot_size);CHKERRV(ierr);
    ierr = MatSetType(L, MATDENSE     );CHKERRV(ierr);
    ierr = MatSetUp(L);CHKERRV(ierr);

	Vec d_i;

	ierr = VecCreate(PETSC_COMM_WORLD, &d_i);CHKERRV(ierr);
    ierr = VecSetSizes(d_i,PETSC_DECIDE, tot_size);CHKERRV(ierr);    
    ierr = VecSetType(d_i, VECSEQ);CHKERRV(ierr);    

    int ix[tot_size];
  	std::iota(ix, ix + tot_size, 0);

  	double * vals[tot_size];  	
  	if (L_flag)
  	{
  		ierr = VecGetArray(RT_problem_->I_field_, vals);CHKERRV(ierr);  		
  	}
  	else
  	{
  		ierr = VecGetArray(RT_problem_->S_field_, vals);CHKERRV(ierr);
  	}
  	
	// fill L rows
	for (int i = 0; i < (int)tot_size; ++i)
	{

		std::cout << i << std::endl;

		ierr = VecZeroEntries(d_i);CHKERRV(ierr);               
		ierr = VecSetValue(d_i, i, 1.0, INSERT_VALUES);CHKERRV(ierr);
	    
		if (L_flag)
	  	{
	  		mf_ctx_.formal_solve(RT_problem_->I_field_, d_i, 0.0);
	  	}
	  	else
	  	{
	  		mf_ctx_.update_emission(d_i);  
	  	}
  	    	
	  	ierr = MatSetValues(L,1,&i,tot_size,ix,*vals,INSERT_VALUES);CHKERRV(ierr);
	}

	ierr = MatAssemblyBegin(L,MAT_FINAL_ASSEMBLY);CHKERRV(ierr);
    ierr = MatAssemblyEnd(L,MAT_FINAL_ASSEMBLY);CHKERRV(ierr);

	if (L_flag)
  	{
  		save_mat(L, "../output/L.m" ,"Lam"); 
  	}
  	else
  	{
  		save_mat(L, "../output/S.m" ,"Sigma"); 
  	}

	ierr = MatDestroy(&L);CHKERRV(ierr);
	ierr = VecDestroy(&d_i);CHKERRV(ierr);
};

void RT_solver::assemble_rhs(){

  	if (mpi_rank_ == 0) std::cout << "\nAssembling right hand side...";

	PetscErrorCode ierr;

	const auto N_nu       = RT_problem_->N_nu_;
	const auto       size = RT_problem_->tot_size_;
	const auto block_size = RT_problem_->block_size_;
	const auto eta_field  = RT_problem_->eta_field_;

	Vec eps_th; // TODO use directly rhs_
	double eta_i, eta_I, value;
	size_t i_space;
	int index_I, index_s_nu;

	ierr = VecCreate(PETSC_COMM_WORLD, &eps_th);CHKERRV(ierr);    
	ierr = VecSetSizes(eps_th,PETSC_DECIDE,size);CHKERRV(ierr);   
	ierr = VecSetBlockSize(eps_th,block_size);CHKERRV(ierr);
	ierr = VecSetFromOptions(eps_th);CHKERRV(ierr);

	int istart, iend;
	ierr = VecGetOwnershipRange(eta_field, &istart, &iend);CHKERRV(ierr);

	// fill eps_th =  eps_c_th +  eps_l_th
	for (int i = istart; i < iend; ++i)
	{	
		value = 0.0;

		std::vector<size_t> local_idx = RT_problem_->local_to_global(i);
		i_space = local_idx[0];

		if (LTE_) // eps = W_T
		{
			if (local_idx[4] == 0) value = RT_problem_->W_T_[i_space]; // TODO divide by eta_I?
		}
		else // eps = (eps_c_th + eps_l_th_) / eta_I
		{
			// get eta_i		
			ierr = VecGetValues(eta_field, 1, &i, &eta_i);
			
			// first Stokes parameter
			if (local_idx[4] == 0)				
			{	
				index_s_nu = N_nu * i_space + local_idx[3];
				
				if (RT_problem_->enable_continuum_) 
				{
					// eps_c_th
					value = (RT_problem_->eps_c_th_[index_s_nu]);	

					// eps_l_th		
					value += (RT_problem_->epsilon_[i_space]) * (RT_problem_->W_T_[i_space]) * (eta_i - RT_problem_->k_c_[index_s_nu]);				
				}
				else
				{
					// eps_l_th
					value += (RT_problem_->epsilon_[i_space]) * (RT_problem_->W_T_[i_space]) * eta_i;				
				}

				// (eps_c_th + eps_l_th_) / eta_I
				value /= eta_i;
			} 
			else
			{
				// get eta_I (!= eta_i)
				index_I = i - local_idx[4];
				ierr = VecGetValues(eta_field, 1, &index_I, &eta_I);

				// eps_l_th / eta_i_l
				value = eta_i * (RT_problem_->epsilon_[i_space]) * (RT_problem_->W_T_[i_space]) / eta_I;				
			}			
		}
		
		ierr = VecSetValue(eps_th,i,value,INSERT_VALUES);CHKERRV(ierr);   
	}

	ierr = VecAssemblyBegin(eps_th);CHKERRV(ierr); 
	ierr = VecAssemblyEnd(eps_th);CHKERRV(ierr);

	// save_vec(eps_th, "../output/epsth.m" ,"eps_th"); 
	
	// rhs assembly
	ierr = VecCreate(PETSC_COMM_WORLD, &rhs_);CHKERRV(ierr);    
	ierr = VecSetSizes(rhs_,PETSC_DECIDE,size);CHKERRV(ierr);   
	ierr = VecSetBlockSize(rhs_,block_size);CHKERRV(ierr);
	ierr = VecSetFromOptions(rhs_);CHKERRV(ierr);

	// fill rhs_ from formal solve with bc
	mf_ctx_.formal_solve(rhs_, eps_th, 1.0); 	
	
	// clean
	ierr = VecDestroy(&eps_th);CHKERRV(ierr);

	if (mpi_rank_ == 0) std::cout << "done" << std::endl;	
}


// set initial guess for first stokes parameter
void RT_solver::set_I_from_input(const std::string input_path, Vec &I)
{
	if (mpi_rank_ == 0) std::cout << "Reading input initial guess from " << input_path << "/StokesI" << std::endl;

	PetscErrorCode ierr;
	
	const auto N_theta = RT_problem_->N_theta_;
	const auto N_chi   = RT_problem_->N_chi_;
	const auto N_nu    = RT_problem_->N_nu_;

	const auto block_size = RT_problem_->block_size_;

	int istart, iend;
	ierr = VecGetOwnershipRange(I, &istart, &iend);CHKERRV(ierr);	

	const int istart_local = istart / block_size;
	const int iend_local   = iend  / block_size;

	std::string filename;
	
	for (int i = istart_local; i < iend_local; ++i)
	{
		std::stringstream ss;

		if (i < 10)
		{
			ss << input_path << "/StokesI/StokesI_00" << i << ".dat";			
		}	
		else
		{
			ss << input_path << "/StokesI/StokesI_0" << i << ".dat";
		}

		filename = ss.str();
		
		std::ifstream myFile(filename);

		if (not myFile.good()) std::cerr << "\nERROR: File " << filename << " does not exist!\n" << std::endl;

		// read each line (size N_nu)
		std::string line;	

		// direction indeces (for each line)
		size_t j, k, direction = 0;

		// global position and value;
		size_t global_index;
		double I_value;			

		while(getline(myFile, line))
		{
			if (direction >= N_theta * N_chi)  std::cerr << "\nERROR input file: direction >= N_theta * N_chi!" << std::endl;

			std::istringstream lineStream(line);

			for (size_t n = 0; n < N_nu; ++n)
			{				
				lineStream >> I_value;	

				j = direction / N_chi;
				k = direction % N_chi;

				global_index = RT_problem_->local_to_global(i, j, k, n);

				ierr = VecSetValue(I, global_index, I_value, INSERT_VALUES);CHKERRV(ierr);						
			}						

			direction++;			
		} 				
	}	

	ierr = VecAssemblyBegin(I);CHKERRV(ierr); 
	ierr = VecAssemblyEnd(I);CHKERRV(ierr); 
}


void RT_solver::set_Jacobi_prec(){

	if (mpi_rank_ == 0) std::cout << "\nComputing Jacobi preconditioner...";
	
	PetscErrorCode ierr;

	const auto   tot_size = RT_problem_->tot_size_;
	const auto block_size = RT_problem_->block_size_;

	double Ad_ii, P_ii;

    Vec d_i, Ad_i;

    ierr = VecCreate(PETSC_COMM_WORLD, &d_i);CHKERRV(ierr);        
    ierr = VecSetSizes(d_i,PETSC_DECIDE, tot_size);CHKERRV(ierr);      
    ierr = VecSetBlockSize(d_i,block_size);CHKERRV(ierr);
    ierr = VecSetFromOptions(d_i);CHKERRV(ierr);

    ierr = VecCreate(PETSC_COMM_WORLD, &Ad_i);CHKERRV(ierr);        
    ierr = VecSetSizes(Ad_i,PETSC_DECIDE, tot_size);CHKERRV(ierr);      
    ierr = VecSetBlockSize(Ad_i,block_size);CHKERRV(ierr);
    ierr = VecSetFromOptions(Ad_i);CHKERRV(ierr);

	// create Jacobi_prec_
	ierr = VecCreate(PETSC_COMM_WORLD, &Jacobi_prec_);CHKERRV(ierr);		
	ierr = VecSetSizes(Jacobi_prec_,PETSC_DECIDE,tot_size);CHKERRV(ierr);		
	ierr = VecSetBlockSize(Jacobi_prec_,block_size);CHKERRV(ierr);
	ierr = VecSetFromOptions(Jacobi_prec_);CHKERRV(ierr);
	
	int istart, iend;
	ierr = VecGetOwnershipRange(Jacobi_prec_, &istart, &iend);CHKERRV(ierr);

	for (int i = istart; i < iend; ++i)
	{
		if (mpi_rank_ == 0) std::cout << "i = " << i << std::endl;

		ierr = VecZeroEntries(d_i);CHKERRV(ierr);		
		ierr = VecSetValue(d_i, i, 1.0, INSERT_VALUES);CHKERRV(ierr);                    
    	ierr = VecAssemblyBegin(d_i);CHKERRV(ierr); 
    	ierr = VecAssemblyEnd(d_i);CHKERRV(ierr); 

    	ierr = MatMult(MF_operator_approx_,d_i,Ad_i);CHKERRV(ierr); // use CRD or Jacobi 

	    ierr = VecGetValues(Ad_i,1,&i,&Ad_ii);CHKERRV(ierr);

		// compute diagonal value
		P_ii = 1.0 / Ad_ii;

		// store it in Jacobi_prec_
		ierr = VecSetValue(Jacobi_prec_,i,P_ii, INSERT_VALUES);CHKERRV(ierr);
	}

	ierr = VecAssemblyBegin(Jacobi_prec_);CHKERRV(ierr); 
	ierr = VecAssemblyEnd(Jacobi_prec_);CHKERRV(ierr); 	

	ierr = VecDestroy(&d_i);CHKERRV(ierr);
    ierr = VecDestroy(&Ad_i);CHKERRV(ierr);
	
	if (mpi_rank_ == 0) std::cout << "done" << std::endl;
}


// matrix-free matrix vector multiplication y = (Id - LJ)x
PetscErrorCode UserMult(Mat mat,Vec x,Vec y){

	PetscErrorCode ierr; 

	void *ptr;
   	MatShellGetContext(mat,&ptr);
  	MF_context *mf_ctx_ = (MF_context *)ptr;  	

  	// compute new emission in S_field_
  	mf_ctx_->update_emission(x);   	

  	// fill rhs_ from formal solve with zero bc
  	if (mf_ctx_->threaded_)
  	{
  		mf_ctx_->formal_solve_threaded(y, mf_ctx_->RT_problem_->S_field_, 0.0);  		
  	}
  	else
  	{
		mf_ctx_->formal_solve(y, mf_ctx_->RT_problem_->S_field_, 0.0);
  	}

	// update I_out = I_in - I_fs (y = x - y)
	ierr = VecAYPX(y, -1.0, x);CHKERRQ(ierr);

  	return ierr;
}


// matrix-free matrix vector multiplication y = (Id - LJ)x
PetscErrorCode UserMult_approx(Mat mat,Vec x,Vec y){

	PetscErrorCode ierr; 

	void *ptr;
   	MatShellGetContext(mat,&ptr);
  	MF_context *mf_ctx_ = (MF_context *)ptr;

  	// compute new emission in S_field_
  	mf_ctx_->update_emission(x, true); 
  	
  	// fill rhs_ from formal solve with zero bc
	mf_ctx_->formal_solve(y, mf_ctx_->RT_problem_->S_field_, 0.0);
	
	// update I_out = I_in - I_fs (y = x - y)
	ierr = VecAYPX(y, -1.0, x);CHKERRQ(ierr);

  	return ierr;
}


PetscErrorCode MF_pc_Destroy(PC pc){

	PetscErrorCode ierr;

	MF_context *mf_ctx;

	ierr = PCShellGetContext(pc,(void**)&mf_ctx); CHKERRQ(ierr);   
    ierr = PetscFree(mf_ctx);

    // TODO destroy?

	return ierr;

}

PetscErrorCode MF_pc_Apply(PC pc,Vec x,Vec y){

	PetscErrorCode ierr;

	MF_context *mf_ctx;

	ierr = PCShellGetContext(pc,(void**)&mf_ctx);CHKERRQ(ierr);   

	// apply	
	ierr = KSPSolve(mf_ctx->pc_solver_, x, y);CHKERRQ(ierr);

	// print  iterations 
	int iterations;
	ierr = KSPGetIterationNumber(mf_ctx->pc_solver_, &iterations);CHKERRQ(ierr);
	if (mf_ctx->mpi_rank_ == 0) std::cout << "Preconditioner iterations: " << iterations << std::endl;
	
	return ierr;
}


// for Jacobi test
PetscErrorCode MF_Jacobi_Destroy(PC pc){

	PetscErrorCode ierr;

	MF_context *mf_ctx;

	ierr = PCShellGetContext(pc,(void**)&mf_ctx); CHKERRQ(ierr);   
    ierr = PetscFree(mf_ctx);

    // TODO other stuff to destroy?

	return ierr;

}

PetscErrorCode MF_Jacobi_Apply(PC pc,Vec x,Vec y){

	PetscErrorCode ierr;

	MF_context *mf_ctx;

	ierr = PCShellGetContext(pc,(void**)&mf_ctx);CHKERRQ(ierr);   

	// apply
	ierr = VecPointwiseMult(y, x, *mf_ctx->Jacobi_prec_);CHKERRQ(ierr);   

	return ierr;

}


