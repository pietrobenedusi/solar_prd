#ifndef RT_solver_hpp
#define RT_solver_hpp

#include "Formal_solver.hpp"
#include "RT_problem.hpp"
#include <rii_emission_coefficient.h>
#include <thread>

typedef const std::string input_string;

extern PetscErrorCode UserMult(Mat mat,Vec x,Vec y);
extern PetscErrorCode UserMult_approx(Mat mat,Vec x,Vec y);
extern PetscErrorCode MF_pc_Destroy(PC pc);
extern PetscErrorCode MF_pc_Apply(PC pc,Vec x,Vec y);

extern PetscErrorCode MF_Jacobi_Destroy(PC pc);
extern PetscErrorCode MF_Jacobi_Apply(PC pc,Vec x,Vec y);

// matrix-free (MF) structure
struct MF_context {

	std::shared_ptr<RT_problem> RT_problem_;	

	Formal_solver formal_solver_;

	// preconditioner data structures 
	KSP pc_solver_;

	Vec * Jacobi_prec_;

	// MPI varables
	int mpi_rank_;
	int mpi_size_;

	bool threaded_;

	// pointer for emission module and offset
	std::shared_ptr<rii_include::emission_coefficient_computation> ecc_sh_ptr_;
	rii_include::offset_function_cartesian offset_f_;	
	rii_include::emission_coefficient_computation::compute_height_function_type epsilon_computation_function_;
	rii_include::emission_coefficient_computation::compute_height_function_type epsilon_computation_function_approx_;
	rii_include::emission_coefficient_computation::compute_height_function_type epsilon_computation_function_approx2_;

	// formal solvers methods 	
	void formal_solve(Vec &I_field, const Vec &S_field, const double I0);	
	void formal_solve_threaded(Vec &I_field, const Vec &S_field, const double I0);
	void formal_solve_single_ray(Vec &I_field, const Vec &S_field, const size_t j, const size_t k, const size_t n); 
	
	// TODO
	void formal_solve_3D(Vec &I_field, const Vec &S_field, const double I0); 

	// if I < 0 set I = 0	
	void make_intensity_positive(Vec &I_field);
	
	// emission module from Simone
	void set_up_emission_module();
		
	// update emission in all spatial points (given the current I_field_, update S_field_)
	void update_emission(const Vec &I_field, const bool approx = false);
	
	// to get epsilon at the end
	void get_emissivity(const Vec &I_field);
};

class RT_solver
{
public:
	RT_solver(const std::shared_ptr<RT_problem> RT_problem, const input_string formal_solver = "implicit_Euler", const bool using_prec = true, const bool threaded = false) 
	{		
		// assign MPI varaibles and init mf_ctx_
    	MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank_);
    	MPI_Comm_size(MPI_COMM_WORLD, &mpi_size_);  

    	RT_problem_ = RT_problem;  
    	using_prec_ = using_prec;    	

    	mf_ctx_.RT_problem_    = RT_problem;  
    	mf_ctx_.mpi_rank_      = mpi_rank_;
    	mf_ctx_.mpi_size_      = mpi_size_;
    	mf_ctx_.threaded_      = threaded;
    	mf_ctx_.formal_solver_ = Formal_solver(formal_solver);     
    	mf_ctx_.set_up_emission_module();  	    		

    	// assemble rhs
    	assemble_rhs();
    	// save_vec(rhs_, "../output/rhs.m" ,"rhs_prd");          	
    	
    	// set linear system
    	PetscErrorCode ierr;

		int local_size;
		ierr = VecGetLocalSize(rhs_, &local_size);CHKERRV(ierr); 		

		// init user defined Mat mult
		ierr = MatCreateShell(PETSC_COMM_WORLD,local_size,local_size,RT_problem_->tot_size_,RT_problem_->tot_size_,(void*)&mf_ctx_,&MF_operator_);CHKERRV(ierr); 
		ierr = MatShellSetOperation(MF_operator_,MATOP_MULT,(void(*)(void))UserMult);CHKERRV(ierr);
    	
    	ierr = KSPCreate(PETSC_COMM_WORLD,&ksp_solver_);CHKERRV(ierr);
    	ierr = KSPSetOperators(ksp_solver_,MF_operator_,MF_operator_);CHKERRV(ierr);	    		
    	ierr = KSPSetType(ksp_solver_,ksp_type_);CHKERRV(ierr);     	

    	// set preconditioner
    	ierr = KSPGetPC(ksp_solver_,&pc_);CHKERRV(ierr);    		    	    		

    	if (using_prec_)
    	{    	
    		// set MF_operator_approx_		
			ierr = MatCreateShell(PETSC_COMM_WORLD,local_size,local_size,RT_problem_->tot_size_,RT_problem_->tot_size_,(void*)&mf_ctx_,&MF_operator_approx_);CHKERRV(ierr); 
			ierr = MatShellSetOperation(MF_operator_approx_,MATOP_MULT,(void(*)(void))UserMult_approx);CHKERRV(ierr);		

    		if (use_jacobi_prec)
    		{
    			if (mpi_rank_ == 0) std::cout << "\nUsing Jacobi preconditioning..."<< std::endl;

    			set_Jacobi_prec();
	    		mf_ctx_.Jacobi_prec_ = &Jacobi_prec_;

	    		ierr = PCSetType(pc_,PCSHELL);CHKERRV(ierr);
				ierr = PCShellSetContext(pc_, &mf_ctx_);CHKERRV(ierr);		
				ierr = PCShellSetApply(pc_,MF_Jacobi_Apply);CHKERRV(ierr);				
				ierr = PCShellSetDestroy(pc_,MF_Jacobi_Destroy);CHKERRV(ierr);
    		}
    		else
    		{	    		
				// set PC solver 
				ierr = KSPCreate(PETSC_COMM_WORLD,&mf_ctx_.pc_solver_);CHKERRV(ierr);
	    		ierr = KSPSetOperators(mf_ctx_.pc_solver_,MF_operator_approx_,MF_operator_approx_);CHKERRV(ierr);	    		
	    		ierr = KSPSetType(mf_ctx_.pc_solver_,ksp_type_);CHKERRV(ierr); 
	    		
	    		// const double r_tol = 1e-3;
    			// ierr = KSPSetFromOptions(mf_ctx_.pc_solver_);CHKERRV(ierr);    		
    			// ierr = KSPSetTolerances(mf_ctx_.pc_solver_,r_tol,PETSC_DEFAULT,PETSC_DEFAULT, PETSC_DEFAULT);CHKERRV(ierr);	    	

	    		// set PC
	    		ierr = PCSetType(pc_,PCSHELL);CHKERRV(ierr);
				ierr = PCShellSetContext(pc_, &mf_ctx_);CHKERRV(ierr);		
				ierr = PCShellSetApply(pc_,MF_pc_Apply);CHKERRV(ierr);				
				ierr = PCShellSetDestroy(pc_,MF_pc_Destroy);CHKERRV(ierr);	
    		}			
    	}
    	else
    	{
    		ierr = PCSetType(pc_,PCNONE);CHKERRV(ierr);
    	}
    	
    	// extra options    	
    	ierr = PCSetFromOptions(pc_);CHKERRV(ierr);	
    	ierr = KSPSetFromOptions(ksp_solver_);CHKERRV(ierr);	       	
	}

	// solve linear system
	inline void solve()
	{
		PetscErrorCode ierr;

		if (mpi_rank_ == 0) std::cout << "\nStart linear solve..."<< std::endl;		
		ierr = KSPSolve(ksp_solver_, rhs_, RT_problem_->I_field_);CHKERRV(ierr);		
		
		// update_emission if necessary
		// if (mpi_rank_ == 0) std::cout << "\nUpdating source function..."<< std::endl;
		// mf_ctx_.get_emissivity(RT_problem_->I_field_);   
	}

	inline void apply_formal_solver()
	{
		PetscErrorCode ierr;
		
		if (mpi_rank_ == 0) std::cout << "\nApply formal solver..."<< std::endl;
		ierr = VecZeroEntries(RT_problem_->S_field_);CHKERRV(ierr);
		// ierr = VecSet(RT_problem_->S_field_, 1e-20);CHKERRV(ierr);
		mf_ctx_.formal_solve(RT_problem_->I_field_, RT_problem_->S_field_, 1.0);
	}


	inline void compute_emission()
	{
		PetscErrorCode ierr;
		
		if (mpi_rank_ == 0) std::cout << "\nComputing emission..."<< std::endl;

		mf_ctx_.update_emission(RT_problem_->I_field_);		
	}

	// write solution to file
	inline void write_solution(const std::string out_directory)
	{		
		if (mpi_rank_ == 0) std::cout << "\nWriting solution in " << out_directory << std::endl;

		PetscErrorCode ierr; 

		int istart, iend; 
    	ierr = VecGetOwnershipRange(RT_problem_->I_field_, &istart, &iend);CHKERRV(ierr);	

    	const auto block_size = RT_problem_->block_size_;     
	
		const int istart_local = istart / block_size;
		const int iend_local   = iend   / block_size;

		double height;
		int ix[block_size];

		std::vector<double> input(block_size);        

		// update incoming field and write
    	for (int i = istart_local; i < iend_local; ++i)
    	{
    		height = RT_problem_->depth_grid_[i];

    		// set indeces
	    	std::iota(ix, ix + block_size, i * block_size);

    		// get I field at ith height
    		ierr = VecGetValues(RT_problem_->I_field_, block_size, ix, &input[0]);CHKERRV(ierr);	

    		// update
    		mf_ctx_.ecc_sh_ptr_->update_incoming_field<double>(height, mf_ctx_.offset_f_, input.data());

    		//write
    		mf_ctx_.ecc_sh_ptr_->write_to_files(out_directory, height);
    	}    
	} 

	void set_I_from_input(const std::string input_path, Vec &I);

	// save matrices for Matlab
	void save_Lamda();

	void set_Jacobi_prec();
	
private:	

	// MPI varables
	int mpi_rank_;
	int mpi_size_;

	std::shared_ptr<RT_problem> RT_problem_;	
	
	// MF context
	MF_context mf_ctx_;

	// linear system quantities
	Mat MF_operator_;
	Mat MF_operator_approx_;
	Vec rhs_;

	// diagonal preconditioner
	Vec Jacobi_prec_;
	
	KSP ksp_solver_;
	KSPType ksp_type_ = KSPGMRES;
	PC pc_;

	bool LTE_ = false;
	bool using_prec_;
	bool use_jacobi_prec = false;
			
	// assemble Lam[eps_th] + t
	void assemble_rhs();	
};


#endif 
