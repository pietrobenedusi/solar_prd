#include "RT_solver.hpp"
#include <chrono>  

//  make -j4 && mpirun -n 4 ./main

int main(int argc, char *argv[])
{    
    const bool multi_threaded = false;
    const bool output         = false;
    const bool using_prec     = true;
    const bool initial_guess  = false;

    // init MPI for multipel threads 
    if (multi_threaded)
    {
        int provided;
        MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
        if (provided != MPI_THREAD_MULTIPLE) printf("ERROR: this MPI implementation does not support multiple threads\n");
    }
    
	PetscInitialize(&argc,&argv,(char*)0, NULL);

    int mpi_rank, mpi_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

    // TODO add warning in FS
    // int flag;
    // int* tag_ub_ptr;
    // MPI_Comm_get_attr(MPI_COMM_WORLD, MPI_TAG_UB, &tag_ub_ptr, &flag);
    // if (mpi_rank == 0) std::cout << "max tag = "<< *tag_ub_ptr << std::endl;  

    // For RELEASE run configure and compile PETSc with ./configure --with-debugging=0
    // if (mpi_rank == 0) std::cout << "\n<<<<<< PETSc compiled for debugging >>>>>>>" << std::endl;
    if (mpi_rank == 0 and using_prec) std::cout << "\n+++++ Using preconditioning ++++++" << std::endl;

    const size_t N_theta = atoi(argv[1]);
    const size_t N_chi   = atoi(argv[2]);

    auto t1 = std::chrono::high_resolution_clock::now();

    // const std::string input_path  = "../input/FAL-C/1_B0_V0_12T_8C_99F";
    const std::string input_path  = "../input/FAL-C/1_B0_V0_12T_8C_64F_64";
    const std::string output_path = "../output";

    // create RT environment 
    auto rte_problem_ptr = std::make_shared<RT_problem>(input_path, N_theta, N_chi);	
        
    MPI_Barrier(MPI_COMM_WORLD);   
    auto t2 = std::chrono::high_resolution_clock::now();
    if (mpi_rank == 0) std::cout << "Precomputing took " << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() << " ms\n" << std::endl;
    t1 = std::chrono::high_resolution_clock::now();    
    
	// Create RT solver (FS: "implicit_Euler", "trapezoidal" or "DELO_linear")
	RT_solver rt_solver(rte_problem_ptr, "DELO_linear", using_prec, multi_threaded);

    // test for Simone 
    if (initial_guess and N_theta == 12 and N_chi == 8) rt_solver.set_I_from_input(input_path, rte_problem_ptr->I_field_);         

    rt_solver.solve();
    // rt_solver.apply_formal_solver();    
    // rt_solver.compute_emission();    
    
    t2 = std::chrono::high_resolution_clock::now();
    if (mpi_rank == 0) std::cout << "Execution took " << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() << " ms\n" << std::endl;


    rte_problem_ptr->print_surface_profile(rte_problem_ptr->I_field_, 0, N_theta/2 + 1);
    rte_problem_ptr->print_surface_profile(rte_problem_ptr->I_field_, 1, N_theta/2 + 1);
    rte_problem_ptr->print_surface_profile(rte_problem_ptr->I_field_, 2, N_theta/2 + 1);
    rte_problem_ptr->print_surface_profile(rte_problem_ptr->I_field_, 3, N_theta/2 + 1);
   
    // save_vec(rte_problem_ptr->I_field_, "../output/Iprd.m" ,"I_prd");  
    // save_vec(rte_problem_ptr->S_field_, "../output/Sprd.m" ,"S_prd");    

    // output
    if (output) rt_solver.write_solution(output_path);
    
//    rte_problem_ptr->print_profile(rte_problem_ptr->I_field_, 0, N_theta/2, 0);

    // MPI_Barrier(MPI_COMM_WORLD);
    //if (mpi_rank == 0) std::cout << "====================" << std::endl;

    //rte_problem_ptr->print_profile(rte_problem_ptr->I_field_, 1, N_theta/2, 0);
    
    // MPI_Barrier(MPI_COMM_WORLD);
    
    // for (int proc = 0; proc < mpi_size; ++proc)
    // {
    //     if (mpi_rank == proc)
    //     {         
            // rte_problem_ptr->print_profile(rte_problem_ptr->I_field_, 0, proc, N_theta/2, 0);                
    //     }

    //     MPI_Barrier(MPI_COMM_WORLD);
    // }


    // rte_problem_ptr->print_surface_profile(rte_problem_ptr->I_field_, 0, N_theta/2);
    // rte_problem_ptr->print_surface_profile(rte_problem_ptr->S_field_, 0, N_theta/2);
    // // rte_problem_ptr->print_surface_profile(rte_problem_ptr->I_field_, 0, N_theta/2 + 1);
    // rte_problem_ptr->print_surface_profile(rte_problem_ptr->I_field_, 1, N_theta/2);
    // rte_problem_ptr->print_surface_profile(rte_problem_ptr->I_field_, 1, N_theta/2 + 1);

    // rt_solver.compute_emission();

    // rte_problem_ptr->print_surface_profile(rte_problem_ptr->S_field_, 0, N_theta/2);

        
    // rte_problem_ptr->print_profile(rte_problem_ptr->I_field_, 0, 0, N_theta/2, 0);
    // MPI_Barrier(MPI_COMM_WORLD);
    // rte_problem_ptr->print_profile(rte_problem_ptr->I_field_, 1, 0, N_theta/2, 0);


    // MPI_Barrier(MPI_COMM_WORLD);

    // rte_problem_ptr->print_profile(rte_problem_ptr->S_field_, 0, 43, N_theta/2, 0);
    // MPI_Barrier(MPI_COMM_WORLD);
    // rte_problem_ptr->print_profile(rte_problem_ptr->S_field_, 2, 43, N_theta/2, 0);

    // if (mpi_size == 1 and output)
    // {
    //     save_vec(rte_problem_ptr->I_field_, "../output/I_out_seq.m" ,"I_seq");    
    // }
    // else if (output)
    // {
    //     save_vec(rte_problem_ptr->I_field_, "../output/I_out_par.m" ,"I_par");    
    // }
    
	// destroy Petsc objects?

	PetscFinalize();

	return 0;
}



// Tests:


//     // Test global to local and vicercersa 
//     if (mpi_size > 1) printf("ERROR: test is serial\n");
//     for (int i = 0; i < rte_problem_ptr->tot_size_; ++i)
//     {
//         std::vector<size_t> local_idx;

//         local_idx = rte_problem_ptr->local_to_global(i);

//         size_t j = local_idx[4] + rte_problem_ptr->local_to_global(local_idx[0], local_idx[1], local_idx[2], local_idx[3]);

//         if (i != j) printf("ERROR: bug in local to global\n");
//     }

// // ////////////////////////////////////////////
// void test_comunication_module() {
    
//     // Build module
//     auto fsf_in_sh_ptr =
//     rii_include::formal_solver_factory_from_in_struct::make_formal_solver_factory_from_in_struct_shared_ptr();

//     // auto RT_problem_sh_ptr = std::make_shared<testing_rii::RT_problem>();    
//     // fill_RT(RT_problem_sh_ptr);

//     const size_t N_theta = 12;
//     const size_t N_chi   = 8;

//     auto RT_problem_sh_ptr = std::make_shared<RT_problem>("../input/FAL-C/1_B0_V0_12T_8C_99F", N_theta, N_chi); 

//     rii_include::in_RT_problem_interface<RT_problem> RT_interface;

//     RT_interface.add_models(RT_problem_sh_ptr, fsf_in_sh_ptr);

//     auto ecc_sh_ptr = rii_include::emission_coefficient_computation::make_emission_coefficient_computation_shared_ptr();
//     bool flag = ecc_sh_ptr->build_problem(fsf_in_sh_ptr);

//     if (not flag) {
//         std::cout << "Error" << std::endl;
//         return;
//     }    

//     // Call sequence necessary to compute epsilon

//     auto t1 = std::chrono::high_resolution_clock::now();

//     unsigned int u_size = RT_problem_sh_ptr->nu_grid_.size();
    

//     auto offset_f = rii_include::make_default_offset_function(N_theta, N_chi, u_size);
    
//     const auto block_size = RT_problem_sh_ptr->block_size_;     

//     std::vector<double>  input(block_size, 0.0);
//     std::vector<double> output(block_size, 0.0);

//     for (int i = 0; i < block_size; i = i + 4)
//     {
//         input[i] = 1.0;
//     }

//     // PetscErrorCode ierr; 
//     // int ix[block_size];
    
//     // int istart, iend; 
//     // ierr = VecGetOwnershipRange(RT_problem_sh_ptr->S_field_, &istart, &iend);CHKERRV(ierr); 
    
//     // const int istart_local = istart / block_size;
//     // const int iend_local   = iend   / block_size;

//     // for (int i = istart_local; i < iend_local; ++i)    
//     // {
//         // set indeces
//         // std::iota(ix, ix + block_size, i * block_size);

//         // call Simone module
//         double height = 0.0;

//         // // get I field at ith height
//         // ierr = VecGetValues(I_field, block_size, ix, &input[0]);CHKERRV(ierr);  

//         // set input field
//         ecc_sh_ptr->update_incoming_field<double>(height, offset_f, input.data());

//         ecc_sh_ptr->set_threads_number(1);
//         auto epsilon_grid = ecc_sh_ptr->compute_layer(height);
//         rii_include::make_indices_convertion_function<double>(epsilon_grid, offset_f)(output.data());    

//         for (int i = 1; i < block_size; i = i + 4)
//         {
//             std::cout << output[i] <<  std::endl;
//         }

//         // set S_field_ accordingly         
//         // ierr = VecSetValues(RT_problem_sh_ptr->S_field_, block_size, ix, &output[0], INSERT_VALUES);CHKERRV(ierr);        
//     // }

//     // save_vec(RT_problem_sh_ptr->S_field_, "../output/S_out.m" ,"S");    
    
//     // auto sf_sh_ptr = ecc_sh_ptr->update_incoming_field<double>(height, offset_f, input.data());
//     // // std::vector<std::string> files_names{"/tmp/I.csv", "/tmp/Q.csv", "/tmp/U.csv", "/tmp/V.csv"};
//     // // rii_algorithm::write_spherical_field(*sf_sh_ptr, files_names);

//     // ecc_sh_ptr->set_threads_number(1);
//     // auto epsilon_grid = ecc_sh_ptr->compute_layer(height);
//     // rii_include::make_indices_convertion_function<double>(epsilon_grid, offset_f)(output.data());    

//     // for (int i = 0; i < block_size; i++)
//     // {
//     //     std::cout <<  output[i] << std::endl;
//     // }

//     // set S_field_ accordingly
//     // int ix[block_size];
//     // std::iota(ix, ix + block_size, depth_point * block_size);

//     // VecSetValues(RT_problem_sh_ptr->S_field_, block_size, ix, &output[0], INSERT_VALUES);

//     // auto t2 = std::chrono::high_resolution_clock::now();
//     // if (mpi_rank == 0) std::cout << "\nEmission took " << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() << " ms\n" << std::endl;

// }
//  //////////////////////////////////////////

// int main(int argc, char *argv[]){

//     PetscInitialize(&argc,&argv,(char*)0, NULL);
    
//     test_comunication_module();

//     PetscFinalize();

//     return 0;
// }


    //////////////////////////////////


    // // test Jacobi prec and MatMult 
    // int istart, iend; 
    // ierr = VecGetOwnershipRange(rte_problem_ptr->I_field_, &istart, &iend);CHKERRQ(ierr);   

    // double Jacobi_prec_i, Ad_ii;

    // Vec d_i, Ad_i;

    // ierr = VecCreate(PETSC_COMM_WORLD, &d_i);CHKERRQ(ierr);        
    // ierr = VecSetSizes(d_i,PETSC_DECIDE,rte_problem_ptr->tot_size_);CHKERRQ(ierr);      
    // ierr = VecSetBlockSize(d_i,rte_problem_ptr->block_size_);CHKERRQ(ierr);
    // ierr = VecSetFromOptions(d_i);CHKERRQ(ierr);

    // ierr = VecCreate(PETSC_COMM_WORLD, &Ad_i);CHKERRQ(ierr);        
    // ierr = VecSetSizes(Ad_i,PETSC_DECIDE,rte_problem_ptr->tot_size_);CHKERRQ(ierr);      
    // ierr = VecSetBlockSize(Ad_i,rte_problem_ptr->block_size_);CHKERRQ(ierr);
    // ierr = VecSetFromOptions(Ad_i);CHKERRQ(ierr);

    // // ierr = VecSetValue(d_i, 0, 1.0, INSERT_VALUES);CHKERRQ(ierr);                    
    // // ierr = VecAssemblyBegin(d_i);CHKERRQ(ierr); 
    // // ierr = VecAssemblyEnd(d_i);CHKERRQ(ierr); 

    // // VecSet(d_i, 1.0); 

    // // ierr = MatMult(rt_solver.MF_operator_,d_i, Ad_i);CHKERRQ(ierr);

    // // output
    // // if (mpi_size == 1 and output)
    // // {
    // //     save_vec(Ad_i, "../output/I_out1.m" ,"I1");    
    // // }
    // // else if (mpi_size == 4 and output)
    // // {
    // //     save_vec(Ad_i, "../output/I_out4.m" ,"I4");    
    // // }
                
    // for (int i = istart; i < iend; i = i++)        
    // {
    //     ierr = VecZeroEntries(Ad_i);CHKERRQ(ierr);
    //     ierr = VecZeroEntries(d_i);CHKERRQ(ierr);

    //     // A_ii from Jacobi prec.
    //     ierr = VecGetValues(rt_solver.Jacobi_prec_,1,&i,&Jacobi_prec_i);CHKERRQ(ierr);

    //     // set d_i
    //     ierr = VecSetValue(d_i, i, 1.0, INSERT_VALUES);CHKERRQ(ierr);                    
    //     ierr = VecAssemblyBegin(d_i);CHKERRQ(ierr); 
    //     ierr = VecAssemblyEnd(d_i);CHKERRQ(ierr); 

    //     // A_ii from (A * d_i)_i
    //     ierr = MatMult(rt_solver.MF_operator_,d_i, Ad_i);CHKERRQ(ierr);
    //     ierr = VecGetValues(Ad_i,1,&i,&Ad_ii);CHKERRQ(ierr);
        
    //     std::cout << "i = " << i << std::endl;
    //     std::cout << "Jacobi_prec_i = " << 1.0 / Jacobi_prec_i << std::endl;
    //     std::cout << "Ad_ii = " << Ad_ii << std::endl;
    //     std::cout << "Ad_ii - Jacobi_prec_i = " << Ad_ii - 1.0 / Jacobi_prec_i << std::endl;

    // }



















